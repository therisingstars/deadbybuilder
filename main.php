<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
$path = __DIR__.'\\'; 

require $path.'api\src\vendor\autoload.php';
$configuration = require $path.'api\src\conf\api_settings.php';	
$dependance = new \Slim\Container($configuration);
$app = new \Slim\App($dependance);

$app->add(function($request, $response, callable $next){
    $response = $next($request, $response);
    $response = $response->withHeader('Content-type', 'application/json; charset=utf-8');
    $response = $response->withHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,Authorization");
    $response = $response->withHeader('Access-Control-Allow-Origin', $request->getHeader('Origin'));
    $response = $response->withHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
    $response = $response->withHeader('Access-Control-Allow-Credentials', 'true');
    return $response;
});
$app->options('/{routes:.+}', function ($request, $response, $args){
    return $response;
});

$app->get('/hello/{name}[/]','\deadbybuilder\controllers\deadbybuildercontroller:testroute');

$app->get('/killers[/]','\deadbybuilder\controllers\deadbybuildercontroller:allkillers');
$app->get('/survivors[/]','\deadbybuilder\controllers\deadbybuildercontroller:allsurvivors');

$app->get('/killer/{nom_personnage}[/]','\deadbybuilder\controllers\deadbybuildercontroller:killer');
$app->get('/survivor/{nom_personnage}[/]','\deadbybuilder\controllers\deadbybuildercontroller:survivor');

$app->get('/killerbuilds/{nom_personnage}[/]','\deadbybuilder\controllers\deadbybuildercontroller:killerbuilds');

$app->get('/accueilbuildskillers[/]','\deadbybuilder\controllers\deadbybuildercontroller:accueilbuildskillers');
$app->get('/accueilbuildssurvivors[/]','\deadbybuilder\controllers\deadbybuildercontroller:accueilbuildssurvivors');

$app->get('/survivorbuilds/{nom_personnage}[/]','\deadbybuilder\controllers\deadbybuildercontroller:survivorbuilds');

$app->get('/killerbuild/{nom_personnage}[/]','\deadbybuilder\controllers\deadbybuildercontroller:killerbuild');
$app->get('/survivorbuild/{nom_personnage}[/]','\deadbybuilder\controllers\deadbybuildercontroller:killerbuild');

$app->get('/builds/{id_build}[/]','\deadbybuilder\controllers\deadbybuildercontroller:builds');

$app->get('/build/{id_build}[/]','\deadbybuilder\controllers\deadbybuildercontroller:build');
$app->post('/addbuild[/]','\deadbybuilder\controllers\deadbybuildercontroller:addbuild');
$app->get('/removebuild/{id_build}[/]','\deadbybuilder\controllers\deadbybuildercontroller:removebuild');
$app->post('/editbuild[/]','\deadbybuilder\controllers\deadbybuildercontroller:editbuild');

$app->get('/perks/{id_caracteristique}[/]','\deadbybuilder\controllers\deadbybuildercontroller:perks');
$app->get('/perkcommunekiller[/]','\deadbybuilder\controllers\deadbybuildercontroller:communeperkskiller');
$app->get('/perkcommunesurvivor[/]','\deadbybuilder\controllers\deadbybuildercontroller:communeperkssurvivor');

$app->get('/items[/{id_personnage}]','\deadbybuilder\controllers\deadbybuildercontroller:Selectitems');
$app->get('/items-powers[/]','\deadbybuilder\controllers\deadbybuildercontroller:itemspowers');
$app->get('/addons/{id_item}[/]','\deadbybuilder\controllers\deadbybuildercontroller:Selectaddonbyitem');
$app->get('/offering/{id_caracteristique}[/]','\deadbybuilder\controllers\deadbybuildercontroller:Selectoffering');

$app->post('/inscription[/]','\deadbybuilder\controllers\deadbybuildercontroller:inscription');
$app->post('/connexion[/]','\deadbybuilder\controllers\deadbybuildercontroller:connexion');

$app->post('/contactmail[/]','\deadbybuilder\controllers\deadbybuildercontroller:contactmail');

$app->get('/countbuild/{id_user}[/]','\deadbybuilder\controllers\deadbybuildercontroller:countbuild');
$app->get('/countbuilds[/]','\deadbybuilder\controllers\deadbybuildercontroller:countbuilds');
$app->get('/countusers[/]','\deadbybuilder\controllers\deadbybuildercontroller:countusers');
$app->get('/mostbuild[/]','\deadbybuilder\controllers\deadbybuildercontroller:mostbuild');

$app->get('/buildperuser/{id_utilisateur}[/]','\deadbybuilder\controllers\deadbybuildercontroller:buildperuser');
$app->get('/user/{id_utilisateur}[/]','\deadbybuilder\controllers\deadbybuildercontroller:user');

$app->get('/users[/]','\deadbybuilder\controllers\deadbybuildercontroller:users');
$app->get('/removeuser/{id_user}[/]','\deadbybuilder\controllers\deadbybuildercontroller:removeuser');
$app->post('/edituser[/]','\deadbybuilder\controllers\deadbybuildercontroller:edituser');

$app->get('/gestionbuild[/]','\deadbybuilder\controllers\deadbybuildercontroller:gestionbuild');

$app->get('/killerswithoutpower[/]','\deadbybuilder\controllers\deadbybuildercontroller:killerswithoutpower');
$app->get('/killerswithoutpowers/{id_objet}[/]','\deadbybuilder\controllers\deadbybuildercontroller:killerswithoutpowers');

$app->get('/character/{id_personnage}[/]','\deadbybuilder\controllers\deadbybuildercontroller:selectcharacter');
$app->get('/perk/{id_perk}[/]','\deadbybuilder\controllers\deadbybuildercontroller:selectperk');
$app->get('/item/{id_objet}[/]','\deadbybuilder\controllers\deadbybuildercontroller:selectitem');
$app->get('/addon/{id_addon}[/]','\deadbybuilder\controllers\deadbybuildercontroller:selectaddon');
$app->get('/offerings/{id_objet}[/]','\deadbybuilder\controllers\deadbybuildercontroller:Selectofferings');
$app->get('/selectuser/{id_utilisateur}[/]','\deadbybuilder\controllers\deadbybuildercontroller:Selectuser');

$app->get('/characters[/]','\deadbybuilder\controllers\deadbybuildercontroller:characters');
$app->get('/removecharacter/{id_personnage}[/]','\deadbybuilder\controllers\deadbybuildercontroller:removecharacter');
$app->post('/addcharacter[/]','\deadbybuilder\controllers\deadbybuildercontroller:addcharacter');
$app->post('/editcharacter[/]','\deadbybuilder\controllers\deadbybuildercontroller:editcharacter');

$app->get('/adminperks[/]','\deadbybuilder\controllers\deadbybuildercontroller:adminperks');
$app->get('/removeperk/{id_perk}[/]','\deadbybuilder\controllers\deadbybuildercontroller:removeperk');
$app->post('/addperk[/]','\deadbybuilder\controllers\deadbybuildercontroller:addperk');
$app->post('/editperk[/]','\deadbybuilder\controllers\deadbybuildercontroller:editperk');


$app->get('/adminitems[/]','\deadbybuilder\controllers\deadbybuildercontroller:adminitems');
$app->get('/removeitem/{id_item}[/]','\deadbybuilder\controllers\deadbybuildercontroller:removeitem');
$app->post('/additem[/]','\deadbybuilder\controllers\deadbybuildercontroller:additem');
$app->post('/edititem[/]','\deadbybuilder\controllers\deadbybuildercontroller:edititem');


$app->get('/adminaddons[/]','\deadbybuilder\controllers\deadbybuildercontroller:adminaddons');
$app->get('/removeaddon/{id_addon}[/]','\deadbybuilder\controllers\deadbybuildercontroller:removeaddon');
$app->post('/addaddon[/]','\deadbybuilder\controllers\deadbybuildercontroller:addaddon');
$app->post('/editaddon[/]','\deadbybuilder\controllers\deadbybuildercontroller:editaddon');

$app->get('/adminofferings[/]','\deadbybuilder\controllers\deadbybuildercontroller:adminofferings');
$app->get('/removeoffering/{id_offering}[/]','\deadbybuilder\controllers\deadbybuildercontroller:removeoffering');
$app->post('/addoffering[/]','\deadbybuilder\controllers\deadbybuildercontroller:addoffering');
$app->post('/editoffering[/]','\deadbybuilder\controllers\deadbybuildercontroller:editoffering');

$app->get('/addlike/{id_build}[/]','\deadbybuilder\controllers\deadbybuildercontroller:addlike');

$app->get('/adddislike/{id_build}[/]','\deadbybuilder\controllers\deadbybuildercontroller:adddislike');

$app->run();