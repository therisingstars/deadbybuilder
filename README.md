**Deadbybuilder**

Site permettant aux joueurs débutants et confirmés du jeu DeadByDaylight de proposer des compositions de compétences 
sur les différents protagonistes du jeu.


---

## Initialiser le projet

Voici les étapes à suivre pour initialiser le projet.

1. Importer **deadbybuilder.sql** dans votre SGBD préféré.
2. Modifier les constantes de connexion à la base de données selon vos paramètres dans le fichier **connexionmanager.php** se trouvant dans **/api/src/models/** 
3. Dans le répertoire **site** , exécuter la commande **npm install** 
4. Dans ce même répertoire, exécuter la commande **npm run dev**

---

