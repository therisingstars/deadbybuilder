<?php
namespace deadbybuilder\controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use deadbybuilder\models\connexionmanager as connexionmanager;
use \Ramsey\Uuid\Uuid;
use \Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use \Firebase\JWT\JWT;
use \Firebase\JWT\ExpiredException;
use \Firebase\JWT\SignatureInvalidException ;
use \Firebase\JWT\BeforeValidException;

/**
 * 
 */
class deadbybuildercontroller extends connexionmanager
{


	public function testroute(Request $request, Response $response, array $args)
	{
		$name = $args['name'];
	    $response->getBody()->write("Hello, $name");

	    return $response;
	}
    public function allkillers(Request $request, Response $response, array $args)
	{
        $killer = $this->executerRequete("Select personnages.id_personnage, personnages.nom_personnage, personnages.img_personnage, personnages.nom_nospace_personnage from personnages INNER JOIN caracteristiques on personnages.id_caracteristique =  caracteristiques.id_caracteristique where personnages.id_caracteristique = :killer",[':killer'=>1]);
        foreach ($killer as $row) {
            $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));

	    return $response;
	}
    public function allsurvivors(Request $request, Response $response, array $args)
	{
        $survivor = $this->executerRequete("Select personnages.id_personnage, personnages.nom_personnage, personnages.img_personnage, personnages.nom_nospace_personnage from personnages INNER JOIN caracteristiques on personnages.id_caracteristique =  caracteristiques.id_caracteristique where personnages.id_caracteristique = :surv",[':surv'=>2]);
        foreach ($survivor as $row) {
            $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));

	    return $response;
	}
    public function killer(Request $request, Response $response, array $args)
	{  
		$perso_id = $args['nom_personnage'];
        $perso = $this->executerRequete("SELECT personnages.id_personnage, personnages.nom_personnage, personnages.img_personnage, personnages.description_personnage, personnages.nom_nospace_personnage FROM personnages WHERE personnages.nom_nospace_personnage = :nom_personnage AND personnages.id_caracteristique = :id_cara",[":nom_personnage"=>$perso_id, ":id_cara"=> 1]);
       foreach ($perso as $row) {
          $data[]=$row;
       }
        $perk = $this->executerRequete("Select perks.nom_perk, perks.img_perk, perks.description_perk from perks LEFT JOIN personnages ON perks.id_personnage = personnages.id_personnage LEFT JOIN caracteristiques ON personnages.id_caracteristique = caracteristiques.id_caracteristique WHERE personnages.nom_nospace_personnage = :id AND caracteristiques.id_caracteristique = :type",[":id" => $perso_id,":type" => 1]);
        foreach ($perk as $row) {
            $data[]=$row;
        }
        $objet = $this->executerRequete("SELECT objets.nom_objet, objets.img_objet, objets.description_objet FROM objets LEFT JOIN pivot_personnages_objets ON pivot_personnages_objets.id_objet = objets.id_objet LEFT JOIN personnages ON pivot_personnages_objets.id_personnage = personnages.id_personnage WHERE objets.id_type_objet = :type_objet AND objets.id_caracteristique = :type AND personnages.nom_nospace_personnage = :id",[":id" => $perso_id,":type" => 1, ":type_objet"=>1]);
        foreach ($objet as $row) {
            $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));
        
	    return $response;
	}
    public function selectuser(Request $request, Response $response, array $args)
	{  
		$perso_id = $args['id_utilisateur'];
        $perso = $this->executerRequete("SELECT id_utilisateur, username, email, Role FROM utilisateurs WHERE id_utilisateur = :id_objet",[":id_objet"=>$perso_id]);
        foreach ($perso as $row) {
           $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));
        return $response;
	}
    public function selectcharacter(Request $request, Response $response, array $args)
	{  
		$perso_id = $args['id_personnage'];
        $perso = $this->executerRequete("SELECT personnages.id_personnage, personnages.nom_personnage, personnages.img_personnage, personnages.description_personnage, personnages.nom_nospace_personnage, personnages.id_caracteristique FROM personnages WHERE personnages.id_personnage = :id_personnage",[":id_personnage"=>$perso_id]);
        foreach ($perso as $row) {
           $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));
        return $response;
	}
    public function selectperk(Request $request, Response $response, array $args)
	{  
		$perso_id = $args['id_perk'];
        $perso = $this->executerRequete("SELECT id_perk, nom_perk, img_perk, description_perk, id_personnage, is_commun, caracteristique_commun FROM perks WHERE id_perk = :id_perk",[":id_perk"=>$perso_id]);
        foreach ($perso as $row) {
           $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));
        return $response;
	}
    public function selectitem(Request $request, Response $response, array $args)
	{  
		$perso_id = $args['id_objet'];
        $perso = $this->executerRequete("SELECT id_objet, nom_objet, img_objet, description_objet, id_caracteristique FROM objets WHERE id_objet = :id_objet AND id_type_objet = :item",[":id_objet"=>$perso_id, ":item"=>1]);
        foreach ($perso as $row) {
           $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));
        return $response;
	}
    public function selectaddon(Request $request, Response $response, array $args)
	{  
		$perso_id = $args['id_addon'];
        $perso = $this->executerRequete("SELECT distinct addons_objets.id_addon, addons_objets.nom_addon, addons_objets.img_addon, addons_objets.description_addon, group_concat( distinct objets.id_objet) as objet, objets.id_caracteristique FROM addons_objets LEFT JOIN pivot_addsobjets_objets ON addons_objets.id_addon = pivot_addsobjets_objets.id_addon LEFT JOIN objets ON pivot_addsobjets_objets.id_objet = objets.id_objet  WHERE addons_objets.id_addon = :id_objet GROUP BY addons_objets.id_addon",[":id_objet"=>$perso_id]);
        
        foreach ($perso as $row) {
           $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));
        return $response;
	}
    public function selectofferings(Request $request, Response $response, array $args)
	{  
		$perso_id = $args['id_objet'];
        $perso = $this->executerRequete("SELECT id_objet, nom_objet, img_objet, description_objet, id_caracteristique FROM objets WHERE id_objet = :id_objet AND id_type_objet = :item",[":id_objet"=>$perso_id, ":item"=>2]);
        foreach ($perso as $row) {
           $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));
        return $response;
	}
    public function killerbuilds(Request $request, Response $response, array $args)
    {
        $perso_id = $args['nom_personnage'];
        $builds = $this->executerRequete("SELECT builds.id_build, builds.nom_build, builds.description_build, builds.date_creation, (builds.nb_like-builds.nb_dislike) as likes, builds.id_utilisateur, builds.id_personnage, builds.is_public, utilisateurs.username, personnages.nom_personnage, personnages.nom_nospace_personnage, personnages.img_personnage, GROUP_CONCAT(perks.nom_perk) AS perks, GROUP_CONCAT(objets.nom_objet) AS objets, GROUP_CONCAT(addons_objets.nom_addon) AS addons FROM builds LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage LEFT JOIN utilisateurs ON builds.id_utilisateur = utilisateurs.id_utilisateur LEFT JOIN pivot_builds_perks ON builds.id_build = pivot_builds_perks.id_build LEFT JOIN perks ON pivot_builds_perks.id_perk = perks.id_perk LEFT JOIN pivot_builds_objets ON builds.id_build = pivot_builds_objets.id_build LEFT JOIN objets ON pivot_builds_objets.id_objet = objets.id_objet LEFT JOIN pivot_builds_addons ON builds.id_build = pivot_builds_addons.id_build LEFT JOIN addons_objets ON pivot_builds_addons.id_addon = addons_objets.id_addon WHERE personnages.nom_nospace_personnage = :nom GROUP BY builds.id_build ORDER BY likes DESC",[":nom"=>$perso_id]);
        foreach ($builds as $row) {
            $data[]=$row;
            $buildid[]=$row;
        } 
        if (isset($buildid)) {
            $perk = $this->executerRequete("Select perks.nom_perk, perks.img_perk, perks.description_perk, builds.id_build FROM perks LEFT JOIN pivot_builds_perks ON perks.id_perk = pivot_builds_perks.id_perk LEFT JOIN builds ON pivot_builds_perks.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage WHERE personnages.nom_nospace_personnage = :id",[":id" => $perso_id]);
            foreach ($perk as $row) {
                $data[]=$row;
            }
            $item = $this->executerRequete("Select objets.nom_objet, objets.img_objet, objets.description_objet, builds.id_build, objets.id_type_objet FROM objets LEFT JOIN pivot_builds_objets ON objets.id_objet = pivot_builds_objets.id_objet LEFT JOIN builds ON pivot_builds_objets.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage WHERE personnages.nom_nospace_personnage = :id AND objets.id_type_objet = :itemid",[":id" => $perso_id, ":itemid" => "1"]);
            foreach ($item as $row) {
                $data[]=$row;
            }
            $addon = $this->executerRequete("Select addons_objets.nom_addon, addons_objets.img_addon, addons_objets.description_addon, builds.id_build FROM addons_objets LEFT JOIN pivot_builds_addons ON addons_objets.id_addon = pivot_builds_addons.id_addon LEFT JOIN builds ON pivot_builds_addons.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage WHERE personnages.nom_nospace_personnage = :id",[":id" => $perso_id]);
            foreach ($addon as $row) {
                $data[]=$row;
            }
            $offering = $this->executerRequete("Select objets.nom_objet, objets.img_objet, objets.description_objet, builds.id_build, objets.id_type_objet FROM objets LEFT JOIN pivot_builds_objets ON objets.id_objet = pivot_builds_objets.id_objet LEFT JOIN builds ON pivot_builds_objets.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage WHERE personnages.nom_nospace_personnage = :id AND objets.id_type_objet = :offeringid",[":id" => $perso_id, ":offeringid" => "2"]);
            foreach ($offering as $row) {
                $data[]=$row;
            }
            $response->getBody()->write(json_encode($data));
        }
        
	    return $response;
    }
    public function accueilbuildskillers(Request $request, Response $response, array $args)
    {
        $builds = $this->executerRequete("SELECT builds.id_build, builds.nom_build, builds.description_build, builds.date_creation, (builds.nb_like-builds.nb_dislike) as likes, builds.id_utilisateur, builds.id_personnage, builds.is_public, utilisateurs.username, personnages.nom_personnage,personnages.id_caracteristique, personnages.nom_nospace_personnage, personnages.img_personnage, GROUP_CONCAT(perks.nom_perk) AS perks, GROUP_CONCAT(objets.nom_objet) AS objets, GROUP_CONCAT(addons_objets.nom_addon) AS addons FROM builds LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage LEFT JOIN utilisateurs ON builds.id_utilisateur = utilisateurs.id_utilisateur LEFT JOIN pivot_builds_perks ON builds.id_build = pivot_builds_perks.id_build LEFT JOIN perks ON pivot_builds_perks.id_perk = perks.id_perk LEFT JOIN pivot_builds_objets ON builds.id_build = pivot_builds_objets.id_build LEFT JOIN objets ON pivot_builds_objets.id_objet = objets.id_objet LEFT JOIN pivot_builds_addons ON builds.id_build = pivot_builds_addons.id_build LEFT JOIN addons_objets ON pivot_builds_addons.id_addon = addons_objets.id_addon WHERE personnages.id_caracteristique = 1 GROUP BY builds.id_build ORDER BY likes DESC");
        foreach ($builds as $row) {
            $data[]=$row;
            $buildid[]=$row;
        } 
        if (isset($buildid)) {
            $perk = $this->executerRequete("Select perks.nom_perk, perks.img_perk, perks.description_perk, builds.id_build FROM perks LEFT JOIN pivot_builds_perks ON perks.id_perk = pivot_builds_perks.id_perk LEFT JOIN builds ON pivot_builds_perks.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage");
            foreach ($perk as $row) {
                $data[]=$row;
            }
            $item = $this->executerRequete("Select objets.nom_objet, objets.img_objet, objets.description_objet, builds.id_build, objets.id_type_objet FROM objets LEFT JOIN pivot_builds_objets ON objets.id_objet = pivot_builds_objets.id_objet LEFT JOIN builds ON pivot_builds_objets.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage WHERE objets.id_type_objet = :itemid",[":itemid" => "1"]);
            foreach ($item as $row) {
                $data[]=$row;
            }
            $addon = $this->executerRequete("Select addons_objets.nom_addon, addons_objets.img_addon, addons_objets.description_addon, builds.id_build FROM addons_objets LEFT JOIN pivot_builds_addons ON addons_objets.id_addon = pivot_builds_addons.id_addon LEFT JOIN builds ON pivot_builds_addons.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage");
            foreach ($addon as $row) {
                $data[]=$row;
            }
            $offering = $this->executerRequete("Select objets.nom_objet, objets.img_objet, objets.description_objet, builds.id_build, objets.id_type_objet FROM objets LEFT JOIN pivot_builds_objets ON objets.id_objet = pivot_builds_objets.id_objet LEFT JOIN builds ON pivot_builds_objets.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage WHERE objets.id_type_objet = :offeringid",[":offeringid" => "2"]);
            foreach ($offering as $row) {
                $data[]=$row;
            }
            $response->getBody()->write(json_encode($data));
        }
        
	    return $response;
    }
    public function accueilbuildssurvivors(Request $request, Response $response, array $args)
    {
        $builds = $this->executerRequete("SELECT builds.id_build, builds.nom_build, builds.description_build, builds.date_creation, (builds.nb_like-builds.nb_dislike) as likes, builds.id_utilisateur, builds.id_personnage, builds.is_public, utilisateurs.username, personnages.nom_personnage,personnages.id_caracteristique, personnages.nom_nospace_personnage, personnages.img_personnage, GROUP_CONCAT(perks.nom_perk) AS perks, GROUP_CONCAT(objets.nom_objet) AS objets, GROUP_CONCAT(addons_objets.nom_addon) AS addons FROM builds LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage LEFT JOIN utilisateurs ON builds.id_utilisateur = utilisateurs.id_utilisateur LEFT JOIN pivot_builds_perks ON builds.id_build = pivot_builds_perks.id_build LEFT JOIN perks ON pivot_builds_perks.id_perk = perks.id_perk LEFT JOIN pivot_builds_objets ON builds.id_build = pivot_builds_objets.id_build LEFT JOIN objets ON pivot_builds_objets.id_objet = objets.id_objet LEFT JOIN pivot_builds_addons ON builds.id_build = pivot_builds_addons.id_build LEFT JOIN addons_objets ON pivot_builds_addons.id_addon = addons_objets.id_addon WHERE personnages.id_caracteristique = 2 GROUP BY builds.id_build ORDER BY likes DESC");
        foreach ($builds as $row) {
            $data[]=$row;
            $buildid[]=$row;
        } 
        if (isset($buildid)) {
            $perk = $this->executerRequete("Select perks.nom_perk, perks.img_perk, perks.description_perk, builds.id_build FROM perks LEFT JOIN pivot_builds_perks ON perks.id_perk = pivot_builds_perks.id_perk LEFT JOIN builds ON pivot_builds_perks.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage");
            foreach ($perk as $row) {
                $data[]=$row;
            }
            $item = $this->executerRequete("Select objets.nom_objet, objets.img_objet, objets.description_objet, builds.id_build, objets.id_type_objet FROM objets LEFT JOIN pivot_builds_objets ON objets.id_objet = pivot_builds_objets.id_objet LEFT JOIN builds ON pivot_builds_objets.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage WHERE objets.id_type_objet = :itemid",[":itemid" => "1"]);
            foreach ($item as $row) {
                $data[]=$row;
            }
            $addon = $this->executerRequete("Select addons_objets.nom_addon, addons_objets.img_addon, addons_objets.description_addon, builds.id_build FROM addons_objets LEFT JOIN pivot_builds_addons ON addons_objets.id_addon = pivot_builds_addons.id_addon LEFT JOIN builds ON pivot_builds_addons.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage");
            foreach ($addon as $row) {
                $data[]=$row;
            }
            $offering = $this->executerRequete("Select objets.nom_objet, objets.img_objet, objets.description_objet, builds.id_build, objets.id_type_objet FROM objets LEFT JOIN pivot_builds_objets ON objets.id_objet = pivot_builds_objets.id_objet LEFT JOIN builds ON pivot_builds_objets.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage WHERE objets.id_type_objet = :offeringid",[":offeringid" => "2"]);
            foreach ($offering as $row) {
                $data[]=$row;
            }
            $response->getBody()->write(json_encode($data));
        }
        
	    return $response;
    }
    public function survivor(Request $request, Response $response, array $args)
	{  
		$perso_id = $args['nom_personnage'];
        $perso = $this->executerRequete("SELECT personnages.id_personnage, personnages.nom_personnage, personnages.img_personnage, personnages.description_personnage, personnages.nom_nospace_personnage FROM personnages WHERE personnages.nom_nospace_personnage = :nom_personnage AND personnages.id_caracteristique = :idcara",[":nom_personnage"=>$perso_id, ":idcara"=> 2]);
        foreach ($perso as $row) {
            $data[] = $row;
        }
        $perk = $this->executerRequete("Select perks.id_perk, perks.nom_perk, perks.img_perk, perks.description_perk from perks LEFT JOIN personnages ON perks.id_personnage = personnages.id_personnage LEFT JOIN caracteristiques ON personnages.id_caracteristique = caracteristiques.id_caracteristique where personnages.nom_nospace_personnage = :id AND caracteristiques.id_caracteristique = :type",[":id" => $perso_id,":type" => 2]);
        foreach ($perk as $row) {
            $data[] = $row;
        }
        $response->getBody()->write(json_encode($data));
        
	    return $response;
	}
    public function survivorbuilds(Request $request, Response $response, array $args)
    {
        $perso_id = $args['nom_personnage'];
        $builds = $this->executerRequete("SELECT builds.id_build, builds.nom_build, builds.description_build, builds.date_creation, (builds.nb_like-builds.nb_dislike) as likes, builds.id_utilisateur, builds.id_personnage, builds.is_public, utilisateurs.username, personnages.nom_personnage, personnages.nom_nospace_personnage, personnages.img_personnage, GROUP_CONCAT(perks.nom_perk) AS perks, GROUP_CONCAT(objets.nom_objet) AS objets, GROUP_CONCAT(addons_objets.nom_addon) AS addons FROM builds LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage LEFT JOIN utilisateurs ON builds.id_utilisateur = utilisateurs.id_utilisateur LEFT JOIN pivot_builds_perks ON builds.id_build = pivot_builds_perks.id_build LEFT JOIN perks ON pivot_builds_perks.id_perk = perks.id_perk LEFT JOIN pivot_builds_objets ON builds.id_build = pivot_builds_objets.id_build LEFT JOIN objets ON pivot_builds_objets.id_objet = objets.id_objet LEFT JOIN pivot_builds_addons ON builds.id_build = pivot_builds_addons.id_build LEFT JOIN addons_objets ON pivot_builds_addons.id_addon = addons_objets.id_addon WHERE personnages.nom_nospace_personnage = :nom GROUP BY builds.id_build ORDER BY likes DESC",[":nom"=>$perso_id]);
        foreach ($builds as $row) {
            $data[]=$row;
            $buildid[]=$row;
        } 
        if (isset($buildid)) {
            $perk = $this->executerRequete("Select perks.nom_perk, perks.img_perk, perks.description_perk, builds.id_build FROM perks LEFT JOIN pivot_builds_perks ON perks.id_perk = pivot_builds_perks.id_perk LEFT JOIN builds ON pivot_builds_perks.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage WHERE personnages.nom_nospace_personnage = :id",[":id" => $perso_id]);
            foreach ($perk as $row) {
                $data[]=$row;
            }
            $item = $this->executerRequete("Select objets.nom_objet, objets.img_objet, objets.description_objet, builds.id_build, objets.id_type_objet FROM objets LEFT JOIN pivot_builds_objets ON objets.id_objet = pivot_builds_objets.id_objet LEFT JOIN builds ON pivot_builds_objets.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage WHERE personnages.nom_nospace_personnage = :id AND objets.id_type_objet = :itemid",[":id" => $perso_id, ":itemid" => "1"]);
            foreach ($item as $row) {
                $data[]=$row;
            }
            $addon = $this->executerRequete("Select addons_objets.nom_addon, addons_objets.img_addon, addons_objets.description_addon, builds.id_build FROM addons_objets LEFT JOIN pivot_builds_addons ON addons_objets.id_addon = pivot_builds_addons.id_addon LEFT JOIN builds ON pivot_builds_addons.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage WHERE personnages.nom_nospace_personnage = :id",[":id" => $perso_id]);
            foreach ($addon as $row) {
                $data[]=$row;
            }
            $offering = $this->executerRequete("Select objets.nom_objet, objets.img_objet, objets.description_objet, builds.id_build, objets.id_type_objet FROM objets LEFT JOIN pivot_builds_objets ON objets.id_objet = pivot_builds_objets.id_objet LEFT JOIN builds ON pivot_builds_objets.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage WHERE personnages.nom_nospace_personnage = :id AND objets.id_type_objet = :offeringid",[":id" => $perso_id, ":offeringid" => "2"]);
            foreach ($offering as $row) {
                $data[]=$row;
            }
            $response->getBody()->write(json_encode($data));
        }
        return $response;
    }
    public function builds(Request $request, Response $response, array $args)
    {
        $perso_id = $args['id_build'];
        $builds = $this->executerRequete("SELECT builds.id_build, builds.nom_build, builds.description_build, builds.date_creation, (builds.nb_like-builds.nb_dislike) as likes, builds.id_utilisateur, builds.id_personnage, builds.is_public, utilisateurs.username, personnages.nom_personnage, personnages.nom_nospace_personnage, personnages.img_personnage, personnages.id_caracteristique, GROUP_CONCAT(perks.nom_perk) AS perks, GROUP_CONCAT(objets.nom_objet) AS objets, GROUP_CONCAT(addons_objets.nom_addon) AS addons FROM builds LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage LEFT JOIN utilisateurs ON builds.id_utilisateur = utilisateurs.id_utilisateur LEFT JOIN pivot_builds_perks ON builds.id_build = pivot_builds_perks.id_build LEFT JOIN perks ON pivot_builds_perks.id_perk = perks.id_perk LEFT JOIN pivot_builds_objets ON builds.id_build = pivot_builds_objets.id_build LEFT JOIN objets ON pivot_builds_objets.id_objet = objets.id_objet LEFT JOIN pivot_builds_addons ON builds.id_build = pivot_builds_addons.id_build LEFT JOIN addons_objets ON pivot_builds_addons.id_addon = addons_objets.id_addon WHERE builds.id_build = :nom GROUP BY builds.id_build ORDER BY likes DESC",[":nom"=>$perso_id]);
        foreach ($builds as $row) {
            $data[]=$row;
            $buildid[]=$row;
        } 
        if (isset($buildid)) {
            $perk = $this->executerRequete("Select perks.id_perk, perks.nom_perk, perks.img_perk, perks.description_perk, builds.id_build FROM perks LEFT JOIN pivot_builds_perks ON perks.id_perk = pivot_builds_perks.id_perk LEFT JOIN builds ON pivot_builds_perks.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage WHERE builds.id_build = :id",[":id" => $perso_id]);
            foreach ($perk as $row) {
                $data[]=$row;
            }
            $item = $this->executerRequete("Select objets.id_objet, objets.nom_objet, objets.img_objet, objets.description_objet, builds.id_build, objets.id_type_objet FROM objets LEFT JOIN pivot_builds_objets ON objets.id_objet = pivot_builds_objets.id_objet LEFT JOIN builds ON pivot_builds_objets.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage WHERE builds.id_build = :id AND objets.id_type_objet = :itemid",[":id" => $perso_id, ":itemid" => "1"]);
            foreach ($item as $row) {
                $data[]=$row;
            }
            $addon = $this->executerRequete("Select addons_objets.id_addon, @n := @n + 1 as naddon, addons_objets.nom_addon, addons_objets.img_addon, addons_objets.description_addon, builds.id_build FROM addons_objets CROSS JOIN (SELECT @n := 0) AS m LEFT JOIN pivot_builds_addons ON addons_objets.id_addon = pivot_builds_addons.id_addon LEFT JOIN builds ON pivot_builds_addons.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage WHERE builds.id_build = :id",[":id" => $perso_id]);
            foreach ($addon as $row) {
                $data[]=$row;
            }
            $offering = $this->executerRequete("Select objets.id_objet, objets.nom_objet, objets.img_objet, objets.description_objet, builds.id_build, objets.id_type_objet FROM objets LEFT JOIN pivot_builds_objets ON objets.id_objet = pivot_builds_objets.id_objet LEFT JOIN builds ON pivot_builds_objets.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage WHERE builds.id_build = :id AND objets.id_type_objet = :offeringid",[":id" => $perso_id, ":offeringid" => "2"]);
            foreach ($offering as $row) {
                $data[]=$row;
            }
            $response->getBody()->write(json_encode($data));
        }
        return $response;
    }
    public function communeperkskiller(Request $request, Response $response, array $args)
	{
        $killercommun = $this->executerRequete("SELECT perks.id_perk,perks.nom_perk,perks.is_commun FROM perks LEFT JOIN personnages ON perks.id_personnage = personnages.id_personnage LEFT JOIN caracteristiques ON personnages.id_caracteristique = caracteristiques.id_caracteristique WHERE perks.is_commun = 1 AND perks.caracteristique_commun = 1");
        foreach ($killercommun as $row) {
            $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));

	    return $response;
	}
    public function communeperkssurvivor(Request $request, Response $response, array $args)
	{
        $survcommun = $this->executerRequete("SELECT perks.id_perk,perks.nom_perk,perks.is_commun FROM perks LEFT JOIN personnages ON perks.id_personnage = personnages.id_personnage LEFT JOIN caracteristiques ON personnages.id_caracteristique = caracteristiques.id_caracteristique WHERE perks.is_commun = 1 AND perks.caracteristique_commun = 2");
        foreach ($survcommun as $row) {
            $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));

	    return $response;
	}
    public function selectcaracteristiquebyidperso($idperso){


       $caracteristiquebyidperso = $this->executerRequete("SELECT personnages.id_caracteristique as caracteristique FROM personnages WHERE personnages.id_personnage = :id_personnage",[":id_personnage"=>$idperso]);
       foreach ($caracteristiquebyidperso as $value) {
          $caracteristique = $value['caracteristique'];
       }
          return $caracteristique;
       


    }

    public function selectcaracteristiquebynameperso($nomperso){


       $caracteristiquebynameperso = $this->executerRequete("SELECT personnages.id_caracteristique as caracteristique FROM personnages WHERE personnages.nom_nospace_personnage = :nom_personnage",[":nom_personnage"=>$nomperso]);
       foreach ($caracteristiquebynameperso as $value) {
          $caracteristique = $value['caracteristique'];
       }
       return $caracteristique;


    }


   public function perks(Request $request, Response $response, array $args)
   {
       $caracteristique = $args['id_caracteristique'];
           $nb_cara=1;
       if ($caracteristique == 'surv' || $caracteristique == 12) {
           $nb_cara=2;
       }
       $selectperks = $this->executerRequete("SELECT perks.id_perk,perks.nom_perk,perks.description_perk,perks.img_perk FROM `perks` WHERE perks.caracteristique_commun=:nb_cara",["nb_cara"=>$nb_cara]);
       foreach ($selectperks as $row) {
           $resultat[]=$row;
       }
       $response->getBody()->write(json_encode($resultat));
       return $response;
   }
    public function build(Request $request, Response $response, array $args)
	{
        $id_build = $args['id_build'];
        $build = $this->executerRequete("SELECT * FROM builds where id_build = :build",[":build"=>$id_build]);
        foreach ($build as $row) {
            $data[]=$row;
        }
        
        $buildperk = $this->executerRequete("SELECT perks.nom_perk FROM perks LEFT JOIN pivot_builds_perks ON perks.id_perk = pivot_builds_perks.id_perk WHERE id_build = :build",[":build"=>$id_build]);
        foreach ($buildperk as $row) {
            $data1[]=$row;
        }
        $builditem = $this->executerRequete("SELECT objets.nom_objet FROM objets LEFT JOIN pivot_builds_objets ON objets.id_objet = pivot_builds_objets.id_objet WHERE id_build = :build",[":build"=>$id_build]);
        foreach ($builditem as $row) {
            $data2[]=$row;
        }
        $buildaddons = $this->executerRequete("SELECT addons_objets.nom_addon FROM addons_objets LEFT JOIN pivot_builds_addons ON addons_objets.id_addon = pivot_builds_addons.id_addon WHERE id_build = :build",[":build"=>$id_build]);
        foreach ($buildaddons as $row) {
            $data3[]=$row;
        }
        if(isset($data)) {
            $response->getBody()->write(json_encode($data)."<br><br>");
            if(isset($data1)) {
                $response->getBody()->write(json_encode($data1)."<br><br>");
                if(isset($data2)) {
                    $response->getBody()->write(json_encode($data2)." -> ".json_encode($data3));
                }
            }
        }

	    return $response;
	}

    public function verifnameexist($name){

        $verifnameexist= $this->executerRequete("SELECT utilisateurs.username FROM utilisateurs where utilisateurs.username = :username",[":username"=>$name]);

        foreach ($verifnameexist as $row) {
            $data[]=$row;
        }
        if (!empty($data)) {
            return $data;
        }
    }


    public function inscription(Request $request, Response $response, array $args)
    {   
        $parsedBody = $request->getParsedBody();
        $id_utilisateur = Uuid::uuid4();
        $username = $parsedBody['username'];
        $usernamesanitized = filter_var($username,FILTER_SANITIZE_SPECIAL_CHARS);
        $password = $parsedBody['password'];
        $passwordsanitized = filter_var($password,FILTER_SANITIZE_SPECIAL_CHARS);
        $email = $parsedBody['email'];
        $emailsanitized = filter_var($email,FILTER_SANITIZE_EMAIL); 
        $options = [    
            'cost' => 9
        ];
        $verifname = $this->verifnameexist($usernamesanitized);
        if (empty($verifname)) {
            $passwordsanitizedcrypte = password_hash($passwordsanitized,PASSWORD_BCRYPT,$options);
            $insertuser = $this->executerRequete("INSERT INTO utilisateurs (id_utilisateur,username, password, email) VALUES (:id_utilisateur,:username, :passwordcrypte, :email)",[":id_utilisateur"=>$id_utilisateur,":username"=>$usernamesanitized,":passwordcrypte"=>$passwordsanitizedcrypte,':email'=>$emailsanitized]);
            return "enregistré";
        }else{
            return "deja existant";
        }


    }   
    public function connexion(Request $request, Response $response, array $args)
    {
        $resultat= array();
        $parsedBody = $request->getParsedBody();
        $uri = $request->getUri();
        $baseUrl = $uri->getBaseUrl();
        $fullUrl = (string) $uri;
        $username = $parsedBody['username'];
        $password = $parsedBody['password'];
        if (!is_null($username) && !is_null($password)) {
            $usernamesanitized = filter_var($username,FILTER_SANITIZE_SPECIAL_CHARS);
            $passwordsanitized = filter_var($password,FILTER_SANITIZE_SPECIAL_CHARS);
            $selectuser = $this->executerRequete("SELECT utilisateurs.id_utilisateur, utilisateurs.password, utilisateurs.email, utilisateurs.username, utilisateurs.role FROM utilisateurs WHERE username=:username",[':username'=>$usernamesanitized]);
            foreach ($selectuser as $row) {
                $resultat[]=$row;
            } 
            if (!empty($resultat)) {
                $passrecup = $resultat[0]['password'];
                $emailrecup = $resultat[0]['email'];
                $username = $resultat[0]['username'];
                $idusername = $resultat[0]['id_utilisateur'];
                $rolerecup = $resultat[0]['role'];
                if (hash_equals($passrecup, crypt($passwordsanitized, $passrecup))) 
                {
                    $secret = "D+x2JD6.e99`HA-E";
                    $token = JWT::encode(
                    [
                        'iss' => $fullUrl,
                        'aud' => $baseUrl,
                        'iat' => time(),
                        'exp' => time() + 3600,
                        'id' => $idusername,
                        'username' => $username 
                    ],
                    $secret, 'HS512');
                    $response = $response->withJson(array('username' => $username , 'email' => $emailrecup , 'id_user' => $idusername, 'token' => $token, 'role' => $rolerecup));
                    return $response;
                }else{
                    $response = $response->withStatus(401);
                    return $response;
                }

            }else{
                $response = $response->withStatus(401);
                return $response;
            }
        }else{
            $response = $response->withStatus(401);
            return $response;
        }

    }     
    public function Selectaddonbyitem(Request $request, Response $response, array $args){
        $id_item = $args['id_item'];
        $data= [];
        $addons = $this->executerRequete("SELECT addons_objets.id_addon,addons_objets.nom_addon,addons_objets.description_addon,addons_objets.img_addon FROM addons_objets LEFT JOIN pivot_addsobjets_objets ON pivot_addsobjets_objets.id_addon = addons_objets.id_addon LEFT JOIN objets ON pivot_addsobjets_objets.id_objet = objets.id_objet WHERE objets.id_objet = :id_item",[":id_item"=>$id_item]);
        if (!empty($addons)) {
            foreach ($addons as $row) {
                $data[]=$row;
            }
            $response->getBody()->write(json_encode($data));
            return $response;
        }else{
            return $data;
        }

    }
    public function itemspowers(Request $request, Response $response, array $args)
    {
            $items = $this->executerRequete("SELECT objets.id_objet,objets.nom_objet,objets.img_objet, objets.id_type_objet, objets.id_caracteristique FROM objets WHERE objets.id_type_objet = :type", [":type" => 1]);
            foreach ($items as $row) {
                $data[]=$row;
            }
            $response->getBody()->write(json_encode($data));
            return $response;
    }
    public function Selectitems(Request $request, Response $response, array $args)
    {
        if (!empty($args)) {
          
            $id_personnage = $args['id_personnage'];
            $items = $this->executerRequete("SELECT objets.id_objet,objets.nom_objet,objets.img_objet FROM objets LEFT JOIN pivot_personnages_objets ON pivot_personnages_objets.id_objet = objets.id_objet
            LEFT JOIN personnages ON pivot_personnages_objets.id_personnage = personnages.id_personnage WHERE personnages.id_personnage = :id_personnage",["id_personnage"=>$id_personnage]);
            foreach ($items as $row) {
                $data[]=$row;
            }
            $response->getBody()->write(json_encode($data));
            return $response;
        }else
        {
            $id_cara='2';
            $id_type_objet='1';
            $items = $this->executerRequete("SELECT objets.id_objet,objets.nom_objet,objets.img_objet,objets.description_objet FROM objets WHERE id_caracteristique = :id_cara AND id_type_objet = :id_type_objet ",["id_cara"=>$id_cara,"id_type_objet"=>$id_type_objet]);
            foreach ($items as $row) {
                $data[]=$row;
            }
            $response->getBody()->write(json_encode($data));

        }
    }

    public function Selectoffering(Request $request, Response $response, array $args){

        $caracteristique = $args['id_caracteristique'];
        $nb_cara = 1;
        $type_objet= 2;
        if ($caracteristique == 'surv') {
            $nb_cara= 2;
        }
        $offering = $this->executerRequete("SELECT objets.id_objet,objets.nom_objet,objets.img_objet,objets.description_objet FROM objets WHERE id_type_objet = :type_objet AND id_caracteristique = :id_cara",[":type_objet"=>$type_objet,":id_cara"=>$nb_cara]);
        foreach ($offering as $row) {
            $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));
    }

    public function addbuild(Request $request, Response $response, array $args)
	{
        $parsedBody = $request->getParsedBody();
        $id_build = Uuid::uuid4();
        $zero = 0;
        $nom_build = $parsedBody['nom_build'];
        $desc_build = $parsedBody['desc_build'];
        $id_user = $parsedBody['id_user'];
        $id_personnage = $parsedBody['id_personnage'];
        $date_crea = date("Y-m-d");
        $id_objet = $parsedBody['id_objet'];
        $id_offrande = $parsedBody['id_offrande'];
        $build = $this->executerRequete("INSERT INTO builds (id_build,nom_build, description_build, date_creation, nb_like, nb_dislike, id_utilisateur, id_personnage) VALUES (:id_build,:nom_build,:desc_build,:date_crea,:zero,:zero,:id_user,:id_personnage)",[':id_build'=>$id_build,':nom_build'=>$nom_build,':desc_build'=>$desc_build,':date_crea'=>$date_crea,':zero'=>$zero,':id_user'=>$id_user,':id_personnage'=>$id_personnage]);

         
        $tmp_id_build = $id_build;
        $array_perk = array('idperk1' =>$parsedBody['id_perk1'],'idperk2' =>$parsedBody['id_perk2'],'idperk3'=>$parsedBody['id_perk3'],'idperk4'=>$parsedBody['id_perk4']);
        foreach ($array_perk as $value) {
            if (!is_null($value)) {
                $insertbuildperk = $this->executerRequete("INSERT INTO `pivot_builds_perks` (`id_build`, `id_perk`) VALUES (:id_build, :id_perk)",[':id_build'=>$tmp_id_build,':id_perk'=>$value]);
            } 
        }
        $arrayobjet = array('id_objet' => $id_objet,'id_offrande'=> $id_offrande);
        foreach ($arrayobjet as $objet) {
            if (!is_null($objet)) {
                $insertbuildobjet = $this->executerRequete("INSERT INTO pivot_builds_objets (id_build, id_objet) VALUES (:id_build,:id_objet)",[':id_build'=>$tmp_id_build,':id_objet'=>$objet]);
            } 
        }
       

        $array_addon = array('idaddon1' =>$parsedBody['id_addon1'],'idaddon2' =>$parsedBody['id_addon2']);

        foreach ($array_addon as $addon) {
            if (!is_null($addon))
            {
                $buildaddon = $this->executerRequete("INSERT INTO pivot_builds_addons (id_build, id_addon) VALUES (:id_build,:id_addon)",[':id_build'=>$tmp_id_build,':id_addon'=>$addon]);
            }  
        }

        return $response;

       /* $nom_build = "fast trapper";
        $desc_build = "fast trap setter trapper";
        $date_crea = "2018-11-20";
        $id_utilisateur = 1;
        $id_personnage = 1;
        
        $perk1 = 11;
        $perk2 = 12;
        $perk3 = 13;
        $perk4 = 14;
        
        $item = 1;
        $addon1 = 55;
        $addon2 = 56;
        
        $build = $this->executerRequete("INSERT INTO builds (nom_build, description_build, date_creation, nb_like, nb_dislike, id_utilisateur, id_personnage) VALUES ('".$nom_build."', '".$desc_build."', '".$date_crea."', 0, 0, ".$id_utilisateur.", ".$id_personnage.")");
        
        $buildnom = $this->executerRequete("SELECT id_build FROM builds WHERE nom_build = :nombuild AND description_build = :descbuild AND date_creation = :datecrea AND id_utilisateur = :idutilisateur AND id_personnage = :idpersonnage", [":nombuild"=>$nom_build, ":descbuild"=>$desc_build, ":datecrea"=>$date_crea, ":idutilisateur"=>$id_utilisateur, ":idpersonnage"=>$id_personnage]);
        foreach ($buildnom as $row) {
            $data[]=$row;
        }
        
        $buildperk1 = $this->executerRequete("INSERT INTO pivot_builds_perks (id_build, id_perk) VALUES (".$data[0]['id_build'].", ".$perk1.")");
        $buildperk2 = $this->executerRequete("INSERT INTO pivot_builds_perks (id_build, id_perk) VALUES (".$data[0]['id_build'].", ".$perk2.")");
        $buildperk3 = $this->executerRequete("INSERT INTO pivot_builds_perks (id_build, id_perk) VALUES (".$data[0]['id_build'].", ".$perk3.")");
        $buildperk4 = $this->executerRequete("INSERT INTO pivot_builds_perks (id_build, id_perk) VALUES (".$data[0]['id_build'].", ".$perk4.")");
        
        $builditem = $this->executerRequete("INSERT INTO pivot_builds_objets (id_build, id_objet) VALUES (".$data[0]['id_build'].", ".$item.")");
        
        $buildaddon1 = $this->executerRequete("INSERT INTO pivot_builds_addons (id_build, id_addon) VALUES (".$data[0]['id_build'].", ".$addon1.")");
        $buildaddon2 = $this->executerRequete("INSERT INTO pivot_builds_addons (id_build, id_addon) VALUES (".$data[0]['id_build'].", ".$addon2.")");
        
	    return $response;*/
	}
    public function removebuild(Request $request, Response $response, array $args)
	{
        $id_build = $args['id_build'];
        $buildaddons = $this->executerRequete("DELETE FROM pivot_builds_addons WHERE id_build= :idbuild", [":idbuild"=>$id_build]);
        $builditem = $this->executerRequete("DELETE FROM pivot_builds_objets WHERE id_build= :idbuild", [":idbuild"=>$id_build]);
        $buildperks = $this->executerRequete("DELETE FROM pivot_builds_perks WHERE id_build= :idbuild", [":idbuild"=>$id_build]);
        $build = $this->executerRequete("DELETE FROM builds WHERE id_build= :idbuild", [":idbuild"=>$id_build]);
        return $response;     
    }
    public function contactmail(Request $request, Response $response, array $args)
    {
        $parsedBody = $request->getParsedBody();
        $name = $parsedBody['name'];
        $mail = $parsedBody['mail'];
        $text = $parsedBody['text'];
        if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) {
	       $passage_ligne = "\r\n";
        } else {
	       $passage_ligne = "\n";
        }
        
        $boundary = "-----=".md5(rand());
        
        $header = "From: \"".$name."\"<".$mail.">".$passage_ligne;
        $header .= "Reply-to: \"".$name."\" <".$mail.">".$passage_ligne;
        $header .= "MIME-Version: 1.0".$passage_ligne;
        $header .= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
        
        $sujet = "Message du Site dead by builder";
        
        $email = "deadbybuilder@gmail.com";
        
        mail($email,$sujet,$text,$header);
    }
    public function countbuild(Request $request, Response $response, array $args)
	{
        $perso_id = $args['id_user'];
        $items = $this->executerRequete("SELECT COUNT(*) AS Count FROM builds INNER JOIN utilisateurs ON builds.id_utilisateur = utilisateurs.id_utilisateur WHERE builds.id_utilisateur = :id_utilisateur OR utilisateurs.username = :username",["id_utilisateur"=>$perso_id, "username" =>$perso_id]);
        foreach ($items as $row) {
            $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));
        return $response;
    }
    public function countbuilds(Request $request, Response $response, array $args)
	{
        $items = $this->executerRequete("SELECT COUNT(*) AS Count FROM builds");
        foreach ($items as $row) {
            $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));
        return $response;
    }
    public function countusers(Request $request, Response $response, array $args)
	{
        $items = $this->executerRequete("SELECT COUNT(*) AS Count FROM utilisateurs");
        foreach ($items as $row) {
            $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));
        return $response;
    }
    public function mostbuild(Request $request, Response $response, array $args)
	{
        $items = $this->executerRequete("SELECT utilisateurs.username, COUNT(builds.id_build) AS CountBuild, (sum(builds.nb_like)-sum(builds.nb_dislike)) as likes FROM builds INNER JOIN utilisateurs ON builds.id_utilisateur = utilisateurs.id_utilisateur GROUP BY builds.id_utilisateur ORDER BY Likes DESC LIMIT 20");
        foreach ($items as $row) {
            $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));
        return $response;
    }
    public function buildperuser(Request $request, Response $response, array $args)
	{
        $perso_id = $args['id_utilisateur'];
        $builds = $this->executerRequete("SELECT builds.id_build, builds.nom_build, builds.description_build, builds.date_creation, (builds.nb_like-builds.nb_dislike) as likes, builds.id_utilisateur, builds.id_personnage, builds.is_public, utilisateurs.username, personnages.nom_personnage, personnages.nom_nospace_personnage, personnages.img_personnage, personnages.id_caracteristique, GROUP_CONCAT(DISTINCT perks.nom_perk) AS perks, GROUP_CONCAT(DISTINCT objets.nom_objet) AS objets, GROUP_CONCAT(DISTINCT addons_objets.nom_addon) AS addons FROM builds LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage LEFT JOIN utilisateurs ON builds.id_utilisateur = utilisateurs.id_utilisateur LEFT JOIN pivot_builds_perks ON builds.id_build = pivot_builds_perks.id_build LEFT JOIN perks ON pivot_builds_perks.id_perk = perks.id_perk LEFT JOIN pivot_builds_objets ON builds.id_build = pivot_builds_objets.id_build LEFT JOIN objets ON pivot_builds_objets.id_objet = objets.id_objet LEFT JOIN pivot_builds_addons ON builds.id_build = pivot_builds_addons.id_build LEFT JOIN addons_objets ON pivot_builds_addons.id_addon = addons_objets.id_addon WHERE builds.id_utilisateur = :nom OR utilisateurs.username = :username GROUP BY builds.id_build ORDER BY likes DESC",[":nom"=>$perso_id, ":username"=>$perso_id]);
        foreach ($builds as $row) {
            $data[]=$row;
        }
        if (isset($data)) {
            $perk = $this->executerRequete("Select perks.nom_perk, perks.img_perk, perks.description_perk, builds.id_build, builds.id_utilisateur FROM perks LEFT JOIN pivot_builds_perks ON perks.id_perk = pivot_builds_perks.id_perk LEFT JOIN builds ON pivot_builds_perks.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage LEFT JOIN utilisateurs ON builds.id_utilisateur = utilisateurs.id_utilisateur WHERE builds.id_utilisateur = :id OR utilisateurs.username = :username",[":id" => $perso_id, ":username" => $perso_id]);
            foreach ($perk as $row) {
                $data[]=$row;
            }
            $item = $this->executerRequete("Select objets.nom_objet, objets.img_objet, objets.description_objet, builds.id_build, objets.id_type_objet, builds.id_utilisateur FROM objets LEFT JOIN pivot_builds_objets ON objets.id_objet = pivot_builds_objets.id_objet LEFT JOIN builds ON pivot_builds_objets.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage LEFT JOIN utilisateurs ON builds.id_utilisateur = utilisateurs.id_utilisateur WHERE (builds.id_utilisateur = :id OR utilisateurs.username = :username) AND objets.id_type_objet = :itemid",[":id" => $perso_id,":username" => $perso_id, ":itemid" => "1"]);
            foreach ($item as $row) {
                $data[]=$row;
            }
            $addon = $this->executerRequete("Select addons_objets.nom_addon, addons_objets.img_addon, addons_objets.description_addon, builds.id_build, builds.id_utilisateur FROM addons_objets LEFT JOIN pivot_builds_addons ON addons_objets.id_addon = pivot_builds_addons.id_addon LEFT JOIN builds ON pivot_builds_addons.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage LEFT JOIN utilisateurs ON builds.id_utilisateur = utilisateurs.id_utilisateur WHERE builds.id_utilisateur = :id OR utilisateurs.username = :username",[":id" => $perso_id, ":username" => $perso_id]);
            foreach ($addon as $row) {
                $data[]=$row;
            }
            $offering = $this->executerRequete("Select objets.nom_objet, objets.img_objet, objets.description_objet, builds.id_build, objets.id_type_objet, builds.id_utilisateur FROM objets LEFT JOIN pivot_builds_objets ON objets.id_objet = pivot_builds_objets.id_objet LEFT JOIN builds ON pivot_builds_objets.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage LEFT JOIN utilisateurs ON builds.id_utilisateur = utilisateurs.id_utilisateur WHERE builds.id_utilisateur = :id OR utilisateurs.username = :username AND objets.id_type_objet = :offeringid",[":id" => $perso_id, ":username" => $perso_id, ":offeringid" => "2"]);
            foreach ($offering as $row) {
                $data[]=$row;
            }
        }
        $response->getBody()->write(json_encode($data));
        return $response;
    }
    public function user(Request $request, Response $response, array $args)
	{
        $nom = $args['id_utilisateur'];
        $username = $this->executerRequete("SELECT utilisateurs.id_utilisateur FROM utilisateurs WHERE utilisateurs.username= :id",[":id" => $nom]);
        foreach ($username as $row) {
            $data=$row;
        }
        $response->getBody()->write(json_encode($data));

	    return $response;
	}
    public function users(Request $request, Response $response, array $args)
	{
        $users = $this->executerRequete("SELECT id_utilisateur, username, email, Role FROM utilisateurs");
        foreach ($users as $row) {
            $data[] = $row;
        }
        $response->getBody()->write(json_encode($data));
	    return $response;
	}
    public function removeuser(Request $request, Response $response, array $args)
	{
        $id_user = $args['id_user'];
        $buildaddons = $this->executerRequete("DELETE pivot_builds_addons FROM pivot_builds_addons INNER JOIN builds ON pivot_builds_addons.id_build = builds.id_build WHERE builds.id_utilisateur = :iduser", [":iduser"=>$id_user]);
        $builditem = $this->executerRequete("DELETE pivot_builds_objets FROM pivot_builds_objets INNER JOIN builds ON pivot_builds_objets.id_build = builds.id_build WHERE builds.id_utilisateur = :iduser", [":iduser"=>$id_user]);
        $buildperks = $this->executerRequete("DELETE pivot_builds_perks FROM pivot_builds_perks INNER JOIN builds ON pivot_builds_perks.id_build = builds.id_build WHERE builds.id_utilisateur = :iduser", [":iduser"=>$id_user]);
        $build = $this->executerRequete("DELETE FROM builds WHERE id_utilisateur= :iduser", [":iduser"=>$id_user]);
        $user = $this->executerRequete("DELETE FROM utilisateurs WHERE id_utilisateur= :iduser", [":iduser"=>$id_user]);
        return $response;
    }
    public function gestionbuild(Request $request, Response $response, array $args)
    {
        $builds = $this->executerRequete("SELECT builds.id_build, builds.nom_build, builds.description_build, builds.date_creation, (builds.nb_like-builds.nb_dislike) as likes, builds.id_utilisateur, builds.id_personnage, builds.is_public, utilisateurs.username, personnages.id_caracteristique, personnages.nom_personnage, personnages.nom_nospace_personnage, personnages.img_personnage, GROUP_CONCAT(perks.nom_perk) AS perks, GROUP_CONCAT(objets.nom_objet) AS objets, GROUP_CONCAT(addons_objets.nom_addon) AS addons FROM builds LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage LEFT JOIN utilisateurs ON builds.id_utilisateur = utilisateurs.id_utilisateur LEFT JOIN pivot_builds_perks ON builds.id_build = pivot_builds_perks.id_build LEFT JOIN perks ON pivot_builds_perks.id_perk = perks.id_perk LEFT JOIN pivot_builds_objets ON builds.id_build = pivot_builds_objets.id_build LEFT JOIN objets ON pivot_builds_objets.id_objet = objets.id_objet LEFT JOIN pivot_builds_addons ON builds.id_build = pivot_builds_addons.id_build LEFT JOIN addons_objets ON pivot_builds_addons.id_addon = addons_objets.id_addon GROUP BY builds.id_build ORDER BY likes DESC");
        foreach ($builds as $row) {
            $data[]=$row;
            $buildid[]=$row;
        } 
        if (isset($buildid)) {
            $perk = $this->executerRequete("Select perks.nom_perk, perks.img_perk, perks.description_perk, builds.id_build FROM pivot_builds_perks LEFT JOIN perks ON perks.id_perk = pivot_builds_perks.id_perk LEFT JOIN builds ON pivot_builds_perks.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage");
            foreach ($perk as $row) {
                $data[]=$row;
            }
            $item = $this->executerRequete("Select objets.nom_objet, objets.img_objet, objets.description_objet, builds.id_build, objets.id_type_objet FROM pivot_builds_objets LEFT JOIN objets ON objets.id_objet = pivot_builds_objets.id_objet LEFT JOIN builds ON pivot_builds_objets.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage WHERE objets.id_type_objet = :itemid",[":itemid" => "1"]);
            foreach ($item as $row) {
                $data[]=$row;
            }
            $addon = $this->executerRequete("Select addons_objets.nom_addon, addons_objets.img_addon, addons_objets.description_addon, builds.id_build FROM pivot_builds_addons LEFT JOIN addons_objets ON addons_objets.id_addon = pivot_builds_addons.id_addon LEFT JOIN builds ON pivot_builds_addons.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage");
            foreach ($addon as $row) {
                $data[]=$row;
            }
            $offering = $this->executerRequete("Select objets.nom_objet, objets.img_objet, objets.description_objet, builds.id_build, objets.id_type_objet FROM pivot_builds_objets LEFT JOIN objets ON objets.id_objet = pivot_builds_objets.id_objet LEFT JOIN builds ON pivot_builds_objets.id_build = builds.id_build LEFT JOIN personnages ON builds.id_personnage = personnages.id_personnage WHERE objets.id_type_objet = :offeringid",[":offeringid" => "2"]);
            foreach ($offering as $row) {
                $data[]=$row;
            }
            $response->getBody()->write(json_encode($data));
        }
        
	    return $response;
    }
    public function characters(Request $request, Response $response, array $args)
	{
        $killer = $this->executerRequete("Select personnages.id_personnage, personnages.nom_personnage, personnages.img_personnage, personnages.nom_nospace_personnage, personnages.description_personnage, personnages.id_caracteristique from personnages INNER JOIN caracteristiques on personnages.id_caracteristique =  caracteristiques.id_caracteristique ORDER BY personnages.id_caracteristique");
        foreach ($killer as $row) {
            $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));

	    return $response;
	}
    public function killerswithoutpower(Request $request, Response $response, array $args)
	{
        $killer = $this->executerRequete("SELECT personnages.id_personnage, personnages.nom_personnage, personnages.img_personnage, personnages.nom_nospace_personnage, personnages.description_personnage, personnages.id_caracteristique FROM personnages WHERE personnages.id_personnage NOT IN (SELECT pivot_personnages_objets.id_personnage FROM pivot_personnages_objets)");
        foreach ($killer as $row) {
            $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));

	    return $response;
	}
    public function killerswithoutpowers(Request $request, Response $response, array $args)
	{
        $killer = $this->executerRequete("SELECT personnages.id_personnage, personnages.nom_personnage, personnages.img_personnage, personnages.nom_nospace_personnage, personnages.description_personnage, personnages.id_caracteristique FROM personnages WHERE personnages.id_personnage NOT IN (SELECT pivot_personnages_objets.id_personnage FROM pivot_personnages_objets)");
        foreach ($killer as $row) {
            $data[]=$row;
        }
        $objet = $args['id_objet'];
        $present = $this->executerRequete("SELECT personnages.id_personnage, personnages.nom_personnage, personnages.img_personnage, personnages.nom_nospace_personnage, personnages.description_personnage, personnages.id_caracteristique FROM personnages INNER JOIN pivot_personnages_objets ON personnages.id_personnage = pivot_personnages_objets.id_personnage WHERE pivot_personnages_objets.id_objet = :idobjet", [":idobjet"=>$objet]);
        foreach ($present as $row) {
            $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));

	    return $response;
	}
    public function removecharacter(Request $request, Response $response, array $args)
	{
        $id_personnage = $args['id_personnage'];
        $build1 = $this->executerRequete("DELETE FROM pivot_builds_perks INNER JOIN builds ON pivot_builds_perks.id_build = builds.id_builds WHERE builds.id_personnage = :idperso", [":idperso" =>$id_personnage]);
        $build2 = $this->executerRequete("DELETE FROM pivot_builds_addons INNER JOIN builds ON pivot_builds_addons.id_build = builds.id_builds WHERE builds.id_personnage = :idperso", [":idperso" =>$id_personnage]);
        $build3 = $this->executerRequete("DELETE FROM pivot_builds_objets INNER JOIN builds ON pivot_builds_objets.id_build = builds.id_builds WHERE builds.id_personnage = :idperso", [":idperso" =>$id_personnage]);
        $build4 = $this->executerRequete("DELETE FROM builds WHERE id_personnage = :idperso", [":idperso" =>$id_personnage]);
        $perks = $this->executerRequete("SELECT id_perk, caracteristique_commun FROM perks WHERE id_personnage = :idperso", [":idperso"=>$id_personnage]);
        foreach ($perks as $row) {
            if ($row['caracteristique_commun'] == 1) {
                $perks = $this->executerRequete("UPDATE `pivot_builds_perks` SET `id_perk`= 1 WHERE id_perk= '".$row['id_perk']."'", [":idperk" =>$id_perk]);
            }
            else {
                $perks = $this->executerRequete("UPDATE `pivot_builds_perks` SET `id_perk`= 40 WHERE id_perk= '".$row['id_perk']."'", [":idperk" =>$id_perk]);
            }
        }
        $objet = $this->executerRequete("SELECT objets.id_objet, objets.id_caracteristique FROM objets LEFT JOIN pivot_personnages_objets ON pivot_personnages_objets.id_objet = objets.id_objet  WHERE id_type_objet = :idperso AND pivot_personnages_objets.id_personnage = :idper", [":idperso"=>1, "idper"=>$id_personnage]);
        foreach ($objet as $row) {
            if ($row['id_caracteristique'] == 1) {
                $addon = $this->executerRequete("SELECT addons_objets.id_addon FROM addons_objets LEFT JOIN pivot_addsobjets_objets ON pivot_addsobjets_objets.id_addon = addons_objets.id_addon WHERE pivot_addsobjets_objets.id_objet = :idperso", ["idperso"=>$row['id_objet']]);
                foreach($addon as $row) {
                    $addons = $this->executerRequete("DELETE FROM pivot_addsobjets_objets WHERE id_objet = :idperso", [":idperso" =>$row['id_objet']]);
                    $addons2 = $this->executerRequete("DELETE FROM addons_objets WHERE id_addon = :idperso", [":idperso" =>$row['id_addon']]);
                }
                $objets = $this->executerRequete("DELETE FROM pivot_personnages_objets WHERE id_personnage = :idperso", [":idperso" =>$id_personnage]);
                $objets2 = $this->executerRequete("DELETE FROM objets WHERE id_personnage = :idperso", [":idperso" =>$row['id_objet']]);  
            }
            else {
                $objets3 = $this->executerRequete("DELETE FROM pivot_personnages_objets WHERE id_personnage = :idperso", [":idperso" =>$id_personnage]);
            }
        }
        $perks2 = $this->executerRequete("DELETE FROM perks WHERE id_personnage = :idperso", [":idperso"=>$id_personnage]);
        $perso = $this->executerRequete("DELETE FROM personnages WHERE id_personnage = :idperso", [":idperso"=>$id_personnage]);
        
        return $response;
    }
    public function addcharacter(Request $request, Response $response, array $args)
	{
        $parsedBody = $request->getParsedBody();
        $name = $parsedBody['name'];
        $desc = str_replace('"', "'", $parsedBody['desc']);
        $namenospace = str_replace(" ", "_", $parsedBody['name']);
        $cara = $parsedBody['type'];
        $basePath = $request->getUri()->getBasePath();
        $host = $request->getUri()->getHost();
        $chemin = $_SERVER["DOCUMENT_ROOT"].$basePath;
        
        if($cara == 1) {
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/killers/";
            $target_dir2 = $chemin."/site/src/assets/img/deadbydaylight/killers/thumbnail/";
        }
        else {
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/survivors/";
            $target_dir2 = $chemin."/site/src/assets/img/deadbydaylight/survivors/thumbnail/";
        }
        $target_file = $target_dir . strtolower($namenospace).'.png';
        $target_file2 = $target_dir2 . strtolower($namenospace).'.png';

        move_uploaded_file($_FILES['image1']["tmp_name"], $target_file);
        move_uploaded_file($_FILES['image2']["tmp_name"], $target_file2);
        
        $character = $this->executerRequete('INSERT INTO `personnages`(`nom_personnage`, `description_personnage`, `img_personnage`, `nom_nospace_personnage`, `id_caracteristique`) VALUES ("'.$name.'","'.$desc.'","'.strtolower($namenospace).'.png","'.$namenospace.'","'.$cara.'")');
        if ($cara == 2) {
        $id = $this->executerRequete("SELECT id_personnage FROM personnages WHERE nom_nospace_personnage = :nomnospace", [":nomnospace" => $namenospace]);
        foreach ($id as $row) {
            $data[]=$row;
        }
        $objets = $this->executerRequete("SELECT id_objet FROM objets WHERE id_type_objet = :idtype AND id_caracteristique = :idcara", [":idtype" =>1, ":idcara" =>2]);
            foreach($objets as $row) {
               $character = $this->executerRequete('INSERT INTO `pivot_personnages_objets`(`id_personnage`, `id_objet`) VALUES ("'.$data[0]['id_personnage'].'","'.$row['id_objet'].'")');
            }
        }
        
	    return $response;
	}
    public function editcharacter(Request $request, Response $response, array $args)
	{
        $parsedBody = $request->getParsedBody();
        $id = $parsedBody['id'];
        $name = $parsedBody['name'];
        $desc = str_replace('"', "'", $parsedBody['desc']);
        $namenospace = str_replace(" ", "_", $parsedBody['name']);
        $cara = $parsedBody['type'];
        $img = $parsedBody['img'];
        $checkimage1 = $parsedBody['checkimage1'];
        $checkimage2 = $parsedBody['checkimage2'];
        $basePath = $request->getUri()->getBasePath();
        $host = $request->getUri()->getHost();
        $chemin = $_SERVER["DOCUMENT_ROOT"].$basePath;
        
        $character = $this->executerRequete('UPDATE `personnages` SET `nom_personnage`="'.$name.'",`description_personnage`="'.$desc.'",`nom_nospace_personnage`="'.$namenospace.'",`id_caracteristique`="'.$cara.'" WHERE id_personnage = :id', [":id" => $id]);
            
        if($cara == 1) {
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/killers/";
            $target_dir2 = $chemin."/site/src/assets/img/deadbydaylight/killers/thumbnail/";
        }
        else {
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/survivors/";
            $target_dir2 = $chemin."/site/src/assets/img/deadbydaylight/survivors/thumbnail/";
        }
        $target_file = $target_dir . $img;
        $target_file2 = $target_dir2 . $img;
        
        if($checkimage1 == true && $_FILES['image1']["tmp_name"] != null) {
            move_uploaded_file($_FILES['image1']["tmp_name"], $target_file);
        }
        if($checkimage2 == true && $_FILES['image2']["tmp_name"] != null) {
            move_uploaded_file($_FILES['image2']["tmp_name"], $target_file2);
        }
        
	    return $response;
	}
    public function adminperks(Request $request, Response $response, array $args)
	{
        $killer = $this->executerRequete("SELECT perks.id_perk, perks.nom_perk, perks.img_perk, perks.description_perk, perks.id_personnage, perks.is_commun, perks.caracteristique_commun, personnages.nom_personnage FROM perks LEFT JOIN personnages ON perks.id_personnage = personnages.id_personnage");
        foreach ($killer as $row) {
            $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));

	    return $response;
	}
    public function removeperk(Request $request, Response $response, array $args)
	{
        $id_perk = $args['id_perk'];
        $perks = $this->executerRequete("SELECT caracteristique_commun FROM perks WHERE id_perk = :idperso", [":idperso"=>$id_perk]);
        foreach ($perks as $row) {
            if ($row['caracteristique_commun'] == 1) {
                $perks = $this->executerRequete("UPDATE `pivot_builds_perks` SET `id_perk`= 1 WHERE id_perk= :idperk", [":idperk" =>$id_perk]);
            }
            else {
                $perks = $this->executerRequete("UPDATE `pivot_builds_perks` SET `id_perk`= 40 WHERE id_perk= :idperk", [":idperk" =>$id_perk]);
            }
        }
        $perks2 = $this->executerRequete("DELETE FROM perks WHERE id_perk = :idperso", [":idperso"=>$id_perk]);
        
        return $response;
    }
    public function addperk(Request $request, Response $response, array $args)
	{
        $parsedBody = $request->getParsedBody();
        $name = $parsedBody['name'];
        $namenospace = str_replace(' ', '_', $parsedBody['name']);
        $desc = str_replace('"', "'", $parsedBody['desc']);
        $cara = $parsedBody['type'];
        $character = $parsedBody['character'];
        $basePath = $request->getUri()->getBasePath();
        $host = $request->getUri()->getHost();
        $chemin = $_SERVER["DOCUMENT_ROOT"].$basePath;
        
        if($cara == 1) {
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/killers/perks/";
        }
        else {
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/survivors/perks/";
        }
        $target_file = $target_dir . strtolower($namenospace).'.png';

        move_uploaded_file($_FILES['image1']["tmp_name"], $target_file);
        
        if($character == "COMMON") {
            $id_perso = null;
            $is_common = 1;
        }
        else {
            $id_perso = $character;
            $is_common = 0;
        }
        $perk = $this->executerRequete('INSERT INTO `perks`(`nom_perk`, `img_perk`, `description_perk`, `id_personnage`, `is_commun`, `caracteristique_commun`) VALUES ("'.$name.'","'.strtolower($namenospace).'.png","'.$desc.'","'.$id_perso.'","'.$is_common.'","'.$cara.'")');
	    return $response;
	}
    public function editperk(Request $request, Response $response, array $args)
	{
        $parsedBody = $request->getParsedBody();
        $id = $parsedBody['id'];
        $name = $parsedBody['name'];
        $desc = str_replace('"', "'", $parsedBody['desc']);
        $namenospace = str_replace(" ", "_", $parsedBody['name']);
        $cara = $parsedBody['type'];
        $img = $parsedBody['img'];
        $character = $parsedBody['character'];
        $checkimage1 = $parsedBody['checkimage1'];
        $basePath = $request->getUri()->getBasePath();
        $host = $request->getUri()->getHost();
        $chemin = $_SERVER["DOCUMENT_ROOT"].$basePath;
        
        if($character == "COMMON") {
          $perk = $this->executerRequete('UPDATE `perks` SET `nom_perk`="'.$name.'",`description_perk`="'.$desc.'",`id_personnage`=NULL, `is_commun`=:com, `caracteristique_commun`="'.$cara.'" WHERE id_perk = :id', [":id" => $id, ":com" => 1]);  
        }
        else {
          $perk = $this->executerRequete('UPDATE `perks` SET `nom_perk`="'.$name.'",`description_perk`="'.$desc.'",`id_personnage`="'.$character.'", `is_commun`=:com, `caracteristique_commun`="'.$cara.'" WHERE id_perk = :id', [":id" => $id, ":com" => 0]);  
        }
            
        if($cara == 1) {
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/killers/perks/";
        }
        else {
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/survivors/perks/";
        }
        $target_file = $target_dir . $img;
        
        if($checkimage1 == true && $_FILES['image1']["tmp_name"] != null) {
            move_uploaded_file($_FILES['image1']["tmp_name"], $target_file);
        }
	    return $response;
	}
    public function adminitems(Request $request, Response $response, array $args)
	{
        $killer = $this->executerRequete("SELECT objets.id_objet, objets.nom_objet, objets.img_objet, objets.description_objet, objets.id_caracteristique, personnages.id_personnage, personnages.nom_personnage FROM objets LEFT JOIN pivot_personnages_objets ON objets.id_objet = pivot_personnages_objets.id_objet LEFT JOIN personnages ON pivot_personnages_objets.id_personnage = personnages.id_personnage WHERE objets.id_type_objet = :type AND objets.id_caracteristique = :idcara", [":type"=>1, ":idcara"=>1]);
        foreach ($killer as $row) {
            $data[]=$row;
        }
        $survivor = $this->executerRequete("SELECT distinct objets.id_objet, objets.nom_objet, objets.img_objet, objets.description_objet, objets.id_caracteristique, 'Survivor' as nom_personnage FROM objets LEFT JOIN pivot_personnages_objets ON objets.id_objet = pivot_personnages_objets.id_objet LEFT JOIN personnages ON pivot_personnages_objets.id_personnage = personnages.id_personnage WHERE objets.id_type_objet = :type AND objets.id_caracteristique = :idcara", [":type"=>1, ":idcara"=>2]);
        foreach ($survivor as $row) {
            $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));

	    return $response;
	}
    public function removeitem(Request $request, Response $response, array $args)
	{
        $id_item = $args['id_item'];
        $objet = $this->executerRequete("SELECT objets.id_caracteristique FROM objets WHERE id_objet = :idperso", ["idperso"=>$id_item]);
        foreach ($objet as $row) {
            if ($row['id_caracteristique'] == 1) {
                $addon = $this->executerRequete("SELECT addons_objets.id_addon FROM addons_objets LEFT JOIN pivot_addsobjets_objets ON pivot_addsobjets_objets.id_addon = addons_objets.id_addon WHERE pivot_addsobjets_objets.id_objet = :idperso", ["idperso"=>$id_item]);
                foreach($addon as $row) {
                    $addons = $this->executerRequete("DELETE FROM pivot_addsobjets_objets WHERE id_objet = :idperso", [":idperso" =>$id_item]);
                    $addons2 = $this->executerRequete("DELETE FROM addons_objets WHERE id_addon = :idperso", [":idperso" =>$row['id_addon']]);
                }
            }
            $build1 = $this->executerRequete("DELETE FROM pivot_builds_addons LEFT JOIN addons_objets ON pivot_builds_addons.id_addon = addons_objets.id_addon LEFT JOIN pivot_addsobjets_objets ON addons_objets.id_addon = pivot_addsobjets_objets.id_addon WHERE pivot_addsobjets_objets.id_objet = :idperso", [":idperso" =>$id_item]);
            $build2 = $this->executerRequete("DELETE FROM pivot_builds_objets WHERE id_objet = :idperso", [":idperso" =>$id_item]);
            $objets = $this->executerRequete("DELETE FROM pivot_personnages_objets WHERE id_objet = :idperso", [":idperso" =>$id_item]);
            $objets2 = $this->executerRequete("DELETE FROM objets WHERE id_personnage = :idperso", [":idperso" =>$id_item]);
        }
        return $response;
    }
    public function additem(Request $request, Response $response, array $args)
	{
        $parsedBody = $request->getParsedBody();
        $name = $parsedBody['name'];
        $namenospace = str_replace(' ', '_', $parsedBody['name']);
        $desc = str_replace('"', "'", $parsedBody['desc']);
        $cara = $parsedBody['type'];
        $character = $parsedBody['character'];
        $basePath = $request->getUri()->getBasePath();
        $host = $request->getUri()->getHost();
        $chemin = $_SERVER["DOCUMENT_ROOT"].$basePath;
        
        if($cara == 1) {
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/killers/items/";
        }
        else {
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/survivors/items/";
        }
        $target_file = $target_dir . strtolower($namenospace).'.png';

        move_uploaded_file($_FILES['image1']["tmp_name"], $target_file);

        $is_common = null;
        $id_type_objet = 1;
        
        $item = $this->executerRequete('INSERT INTO `objets`(`nom_objet`, `description_objet`, `img_objet`, `id_type_objet`, `id_caracteristique`) VALUES ("'.$name.'","'.$desc.'","'.strtolower($namenospace).'.png","'.$id_type_objet.'","'.$cara.'")');
        
        if ($cara == 1) {
        $id = $character;
        $objets = $this->executerRequete("SELECT id_objet FROM objets WHERE id_type_objet = :idtype AND nom_objet = :nomobjet", [":idtype" =>1, ":nomobjet" =>$name]);
            foreach($objets as $row) {
               $char = $this->executerRequete('INSERT INTO `pivot_personnages_objets`(`id_personnage`, `id_objet`) VALUES ("'.$id.'","'.$row['id_objet'].'")');
            }
        }
        else {
            $objets2 = $this->executerRequete("SELECT id_objet FROM objets WHERE id_type_objet = :idtype AND nom_objet = :nombojet", [":idtype" =>1, ":nomobjet" =>$name]);
            foreach($objets2 as $row) {
               $data[] = $row;
            }
            $perso = $this->executerRequete("SELECT id_personnage FROM personnages WHERE id_caracteristique = :nomnospace", [":nomnospace" => 2]);
            foreach ($perso as $row) {
                $char = $this->executerRequete('INSERT INTO `pivot_personnages_objets`(`id_personnage`, `id_objet`) VALUES ("'.$row['id_personnage'].'","'.$data[0]['id_objet'].'")');
            }
        }
        
	    return $response;
	}
    public function edititem(Request $request, Response $response, array $args)
	{
        $parsedBody = $request->getParsedBody();
        $id = $parsedBody['id'];
        $name = $parsedBody['name'];
        $desc = str_replace('"', "'", $parsedBody['desc']);
        $namenospace = str_replace(" ", "_", $parsedBody['name']);
        $cara = $parsedBody['type'];
        $img = $parsedBody['img'];
        if ($cara == 1) {
        $character = $parsedBody['character'];
        }
        $checkimage1 = $parsedBody['checkimage1'];
        $basePath = $request->getUri()->getBasePath();
        $host = $request->getUri()->getHost();
        $chemin = $_SERVER["DOCUMENT_ROOT"].$basePath;
        
        if($cara == 1) {
          $item = $this->executerRequete('DELETE FROM pivot_personnages_objets WHERE id_objet = :idperso', [':idperso'=>$id]);

          $item2 = $this->executerRequete('INSERT INTO `pivot_personnages_objets`(`id_personnage`, `id_objet`) VALUES ("'.$character.'","'.$id.'")'); 
        }
        else {
          $item = $this->executerRequete('DELETE FROM pivot_personnages_objets WHERE id_objet = :idperso', [':idperso'=>$id]);
          $surv = $this->executerRequete('SELECT id_personnage FROM personnages WHERE id_caracteristique = :idcara', [':idcara'=>$cara]);
            foreach ($surv as $row) {
                $item2 = $this->executerRequete('INSERT INTO `pivot_personnages_objets`(`id_personnage`, `id_objet`) VALUES ("'.$row['id_personnage'].'","'.$id.'")');
            }
        }
        $item3 = $this->executerRequete('UPDATE `objets` SET `nom_objet`="'.$name.'",`description_objet`="'.$desc.'", `id_caracteristique`="'.$cara.'" WHERE id_objet = :id', [":id" => $id]);  
        if($cara == 1) {
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/killers/items/";
        }
        else {
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/survivors/items/";
        }
        $target_file = $target_dir . $img;
        
        if($checkimage1 == true && $_FILES['image1']["tmp_name"] != null) {
            move_uploaded_file($_FILES['image1']["tmp_name"], $target_file);
        }
	    return $response;
	}
    public function adminaddons(Request $request, Response $response, array $args)
	{
        $offering = $this->executerRequete("SELECT distinct addons_objets.id_addon, addons_objets.nom_addon, addons_objets.img_addon, addons_objets.description_addon, group_concat( distinct objets.nom_objet) as objet, objets.id_caracteristique FROM addons_objets LEFT JOIN pivot_addsobjets_objets ON addons_objets.id_addon = pivot_addsobjets_objets.id_addon LEFT JOIN objets ON pivot_addsobjets_objets.id_objet = objets.id_objet GROUP BY addons_objets.id_addon");
        foreach ($offering as $row) {
            $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));
        
	    return $response;
	}
    public function removeaddon(Request $request, Response $response, array $args)
	{
        $id_addon = $args['id_addon'];
        $build1 = $this->executerRequete("DELETE FROM pivot_builds_addons WHERE id_addon = :idperso", [":idperso" =>$id_addon]);
        $addons = $this->executerRequete("DELETE FROM pivot_addsobjets_objets WHERE id_addon = :idperso", [":idperso" =>$id_addon]);
        $addons2 = $this->executerRequete("DELETE FROM addons_objets WHERE id_addon = :idperso", [":idperso" =>$id_addon]);

	    return $response;
	}
    public function addaddon(Request $request, Response $response, array $args)
	{
        $parsedBody = $request->getParsedBody();
        $name = $parsedBody['name'];
        $namenospace = str_replace(' ', '_', $parsedBody['name']);
        $desc = str_replace('"', "'", $parsedBody['desc']);
        $cara = $parsedBody['type'];
        $itemss = $parsedBody['itemschecked'];
        $items = explode(',', $itemss);
        $basePath = $request->getUri()->getBasePath();
        $host = $request->getUri()->getHost();
        $chemin = $_SERVER["DOCUMENT_ROOT"].$basePath;
        
        if($cara == 1) {
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/killers/addons/";
        }
        else {
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/survivors/addons/";
        }
        $target_file = $target_dir . strtolower($namenospace).'.png';

        move_uploaded_file($_FILES['image1']["tmp_name"], $target_file);

        $id_type_objet = 1;
        
        $addon = $this->executerRequete('INSERT INTO `addons_objets`(`nom_addon`, `description_addon`, `img_addon`) VALUES ("'.$name.'","'.$desc.'","'.strtolower($namenospace).'.png")');
        
        $addon2 = $this->executerRequete("SELECT id_addon FROM addons_objets WHERE nom_addon = :nomaddon", [":nomaddon" =>$name]);
        foreach($addon2 as $row) {
              $data[] = $row;
            }
        foreach($items as $item) {
                $char = $this->executerRequete('INSERT INTO `pivot_addsobjets_objets`(`id_addon`, `id_objet`) VALUES ("'.$data[0]['id_addon'].'","'.$item.'")');
            }
	    return $response;
	}
    public function editaddon(Request $request, Response $response, array $args)
	{
        $parsedBody = $request->getParsedBody();
        $id = $parsedBody['id'];
        $name = $parsedBody['name'];
        $desc = str_replace('"', "'", $parsedBody['desc']);
        $namenospace = str_replace(" ", "_", $parsedBody['name']);
        $cara = $parsedBody['type'];
        $img = $parsedBody['img'];
        $itemss = $parsedBody['itemschecked'];
        $items = explode(',', $itemss);
        $checkimage1 = $parsedBody['checkimage1'];
        $basePath = $request->getUri()->getBasePath();
        $host = $request->getUri()->getHost();
        $chemin = $_SERVER["DOCUMENT_ROOT"].$basePath;
        
        $addon = $this->executerRequete('DELETE FROM pivot_addsobjets_objets WHERE id_addon = :idperso', [':idperso'=>$id]);
        foreach($items as $item) {
            $char = $this->executerRequete('INSERT INTO `pivot_addsobjets_objets`(`id_addon`, `id_objet`) VALUES ("'.$id.'","'.$item.'")');
        }
        $item3 = $this->executerRequete('UPDATE `addons_objets` SET `nom_addon`="'.$name.'",`description_addon`="'.$desc.'" WHERE id_addon = :id', [":id" => $id]);  
        if($cara == 1) {
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/killers/addons/";
        }
        else {
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/survivors/addons/";
        }
        $target_file = $target_dir . $img;
        
        if($checkimage1 == true && $_FILES['image1']["tmp_name"] != null) {
            move_uploaded_file($_FILES['image1']["tmp_name"], $target_file);
        }
	    return $response;
	}
    public function adminofferings(Request $request, Response $response, array $args)
	{
        $offering = $this->executerRequete("SELECT objets.id_objet, objets.nom_objet, objets.img_objet, objets.description_objet, IFNULL(objets.id_caracteristique, '3') as caracteristique FROM objets WHERE objets.id_type_objet = :type", [":type"=>2]);
        foreach ($offering as $row) {
            $data[]=$row;
        }
        $response->getBody()->write(json_encode($data));

	    return $response;
	}
    public function removeoffering(Request $request, Response $response, array $args)
	{
        $id_objet = $args['id_offering'];
        $build1 = $this->executerRequete("DELETE FROM pivot_builds_objets WHERE id_objet = :idperso", [":idperso" =>$id_objet]);
        $addons2 = $this->executerRequete("DELETE FROM objets WHERE id_objet = :idperso", [":idperso" =>$id_objet]);

	    return $response;
	}
    public function addoffering(Request $request, Response $response, array $args)
	{
        $parsedBody = $request->getParsedBody();
        $name = $parsedBody['name'];
        $namenospace = str_replace(' ', '_', $parsedBody['name']);
        $desc = str_replace('"', "'", $parsedBody['desc']);
        $cara = $parsedBody['type'];
        $basePath = $request->getUri()->getBasePath();
        $host = $request->getUri()->getHost();
        $chemin = $_SERVER["DOCUMENT_ROOT"].$basePath;
        
        if($cara == 1) {
            $is_common = null;
            $id_cara = 1;
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/killers/offerings/";
            $target_file = $target_dir . strtolower($namenospace).'.png';
            move_uploaded_file($_FILES['image1']["tmp_name"], $target_file);
        }
        elseif($cara == 2) {
            $is_common = null;
            $id_cara = 2;
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/survivors/offerings/";
            $target_file = $target_dir . strtolower($namenospace).'.png';
            move_uploaded_file($_FILES['image1']["tmp_name"], $target_file);
        }
        elseif($cara == 3) {
            $is_common = 1;
            $id_cara = null;
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/killers/offerings/";
            $target_file = $target_dir . strtolower($namenospace).'.png';
            $target_dir2 = $chemin."/site/src/assets/img/deadbydaylight/survivors/offerings/";
            $target_file2 = $target_dir2 . strtolower($namenospace).'.png';
            move_uploaded_file($_FILES['image1']["tmp_name"], $target_file);
            copy($target_file , $target_file2);
        }
        $id_type_objet = 2;
        if ($cara == 1) {
            $offering = $this->executerRequete('INSERT INTO `objets`(`nom_objet`, `description_objet`, `img_objet`, `id_type_objet`, `id_caracteristique`) VALUES ("'.$name.'","'.$desc.'","'.strtolower($namenospace).'.png","'.$id_type_objet.'","'.$id_cara.'")');
        }
        elseif ($cara == 2) {
            $offering = $this->executerRequete('INSERT INTO `objets`(`nom_objet`, `description_objet`, `img_objet`, `id_type_objet`, `id_caracteristique`) VALUES ("'.$name.'","'.$desc.'","'.strtolower($namenospace).'.png","'.$id_type_objet.'","'.$id_cara.'")');
        }
        elseif ($cara == 3) {
            $offering = $this->executerRequete('INSERT INTO `objets`(`nom_objet`, `description_objet`, `img_objet`, `id_type_objet`, `is_commun`) VALUES ("'.$name.'","'.$desc.'","'.strtolower($namenospace).'.png","'.$id_type_objet.'","'.$is_common.'")');
        }
        
	    return $response;
	}
    public function editoffering(Request $request, Response $response, array $args)
	{
        $parsedBody = $request->getParsedBody();
        $id = $parsedBody['id'];
        $name = $parsedBody['name'];
        $desc = str_replace('"', "'", $parsedBody['desc']);
        $namenospace = str_replace(" ", "_", $parsedBody['name']);
        $cara = $parsedBody['type'];
        $img = $parsedBody['img'];
        $checkimage1 = $parsedBody['checkimage1'];
        $basePath = $request->getUri()->getBasePath();
        $host = $request->getUri()->getHost();
        $chemin = $_SERVER["DOCUMENT_ROOT"].$basePath;
        
        if($cara == 1) {
            $item3 = $this->executerRequete('UPDATE `objets` SET `nom_objet`="'.$name.'",`description_objet`="'.$desc.'", `id_caracteristique`="'.$cara.'", `is_commun`=NULL WHERE id_objet = :id', [":id" => $id]);
        }
        elseif ($cara == 2) {
            $item3 = $this->executerRequete('UPDATE `objets` SET `nom_objet`="'.$name.'",`description_objet`="'.$desc.'", `id_caracteristique`="'.$cara.'", `is_commun`=NULL WHERE id_objet = :id', [":id" => $id]);
        }
        elseif($cara == 3) {
            $is_common = 1;
            $id_cara = null;
            $item3 = $this->executerRequete('UPDATE `objets` SET `nom_objet`="'.$name.'",`description_objet`="'.$desc.'", `id_caracteristique`=NULL, `is_commun`="'.$is_commun.'" WHERE id_objet = :id', [":id" => $id]);  
        }
        
        if($cara == 1) {
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/killers/offerings/";
            $target_file = $target_dir . $img;
            if($checkimage1 == true && $_FILES['image1']["tmp_name"] != null) {
                move_uploaded_file($_FILES['image1']["tmp_name"], $target_file);
            }
        }
        elseif($cara == 2) {
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/survivors/offerings/";
            $target_file = $target_dir . $img;
            if($checkimage1 == true && $_FILES['image1']["tmp_name"] != null) {
                move_uploaded_file($_FILES['image1']["tmp_name"], $target_file);
            }
        }
        elseif($cara == 3) {
            $target_dir = $chemin."/site/src/assets/img/deadbydaylight/killers/offerings/";
            $target_file = $target_dir . $img;
            $target_dir2 = $chemin."/site/src/assets/img/deadbydaylight/survivors/offerings/";
            $target_file2 = $target_dir2 . $img;
            move_uploaded_file($_FILES['image1']["tmp_name"], $target_file);
            copy($target_file , $target_file2);
        }
        
	    return $response;
	}
    public function edituser(Request $request, Response $response, array $args)
	{
        $parsedBody = $request->getParsedBody();
        $id = $parsedBody['id'];
        $name = $parsedBody['name'];
        $email = $parsedBody['email'];
        $role = $parsedBody['role'];
        $basePath = $request->getUri()->getBasePath();
        $host = $request->getUri()->getHost();
        $chemin = $_SERVER["DOCUMENT_ROOT"].$basePath;
        
        $user = $this->executerRequete('UPDATE `utilisateurs` SET `username`="'.$name.'",`email`="'.$email.'",`Role`="'.$role.'" WHERE id_utilisateur = :id', [":id" => $id]);  

	    return $response;
	}
    public function addlike(Request $request, Response $response, array $args)
	{
        $idbuild = $args['id_build'];
        $username = $this->executerRequete("UPDATE builds set nb_like=nb_like+1 where id_build = :idbuild",[":idbuild" => $idbuild]);

	    return $response;
	}
    public function adddislike(Request $request, Response $response, array $args)
	{
        $idbuild = $args['id_build'];
        $username = $this->executerRequete("UPDATE builds set nb_dislike=nb_dislike+1 where id_build = :idbuild",[":idbuild" => $idbuild]);

	    return $response;
	}
    public function editbuild(Request $request, Response $response, array $args)
	{
        $parsedBody = $request->getParsedBody();
        $id_build = $parsedBody['id_build'];
        $zero = 0;
        $nom_build = $parsedBody['nom_build'];
        $desc_build = $parsedBody['desc_build'];
        $id_user = $parsedBody['id_user'];
        $id_personnage = $parsedBody['id_personnage'];
        $date_crea = date("Y-m-d");
        $id_objet = $parsedBody['id_objet'];
        $id_offrande = $parsedBody['id_offrande'];
        $id_addon1 = $parsedBody['id_addon1'];
        $id_addon2 = $parsedBody['id_addon2'];
        
        
        $build = $this->executerRequete("UPDATE `builds` SET `nom_build`=:nom_build, `description_build`=:desc_build WHERE id_build= :id_build", [":nom_build"=>$nom_build, ":desc_build"=>$desc_build, ":id_build"=>$id_build]);
        
        
        $array_perk = array('idperk1' =>$parsedBody['id_perk1'],'idperk2' =>$parsedBody['id_perk2'],'idperk3'=>$parsedBody['id_perk3'],'idperk4'=>$parsedBody['id_perk4']);
        
        $i = 0;
        $ancienne_perk = $this->executerRequete("SELECT id_perk FROM pivot_builds_perks WHERE id_build = :idbuild", [":idbuild"=>$id_build]);
        foreach ($ancienne_perk as $row) {
            $i = $i + 1;     
            if($row['id_perk'] != $array_perk['idperk'.$i]) {
                $perkbuildedit = $this->executerRequete("UPDATE pivot_builds_perks SET id_perk=:id_new_perk where id_build = :idbuild AND id_perk = :id_ancienne_perk",[":id_new_perk"=>$array_perk['idperk'.$i],":idbuild" => $id_build, ":id_ancienne_perk"=>$row['id_perk']]);
            }
        }
        
        
        if (!is_null($id_objet)) {
            $arrayitemexistant = $this->executerRequete("SELECT pivot_builds_objets.id_objet FROM pivot_builds_objets INNER JOIN objets ON pivot_builds_objets.id_objet = objets.id_objet WHERE pivot_builds_objets.id_build =:idbuild AND objets.id_type_objet =:type", [":idbuild"=>$id_build, ":type"=>1]);
            foreach ($arrayitemexistant as $objetexistant) {
                if (!is_null($objetexistant['id_objet'])) {
                    $objet['id_objet'] = $objetexistant['id_objet'];
                }
            }
            if(!is_null($objet['id_objet'])) {
                if ($id_objet != $objet['id_objet']) {
                    $objetbuildedit = $this->executerRequete("UPDATE pivot_builds_objets SET id_objet=:id_new_objet where id_build = :idbuild AND id_objet = :id_ancien_objet",[":id_new_objet"=>$id_objet,":idbuild" => $id_build, ":id_ancien_objet"=>$objet['id_objet']]);
                }   
            }
            elseif(is_null($objet['id_objet'])) {
                $insertbuildobjet = $this->executerRequete("INSERT INTO pivot_builds_objets (id_build, id_objet) VALUES (:id_build,:id_objet)",[':id_build'=>$id_build,':id_objet'=>$id_objet]);
            }
                
        } 
        
        if (!is_null($id_offrande)) {
            $arrayofferingexistant = $this->executerRequete("SELECT pivot_builds_objets.id_objet FROM pivot_builds_objets INNER JOIN objets ON pivot_builds_objets.id_objet = objets.id_objet WHERE pivot_builds_objets.id_build =:idbuild AND objets.id_type_objet =:type", [":idbuild"=>$id_build, ":type"=>2]);
            foreach ($arrayofferingexistant as $offeringexistant) {
                if (!is_null($offeringexistant['id_objet'])) {
                    $offering['id_objet'] = $offeringexistant['id_objet'];
                }
            }
            if(!is_null($offering['id_objet'])) {
                if ($id_offrande != $offering['id_objet']) {
                    $offeringbuildedit = $this->executerRequete("UPDATE pivot_builds_objets SET id_objet=:id_new_objet where id_build = :idbuild AND id_objet = :id_ancien_objet",[":id_new_objet"=>$id_offrande,":idbuild" => $id_build, ":id_ancien_objet"=>$offering['id_objet']]);
                }
            }
            elseif(is_null($offering['id_objet'])) {
                $insertbuildoffering = $this->executerRequete("INSERT INTO pivot_builds_objets (id_build, id_objet) VALUES (:id_build,:id_objet)",[':id_build'=>$id_build,':id_objet'=>$id_offrande]);
            }
                
        }
        
        if ($id_addon1 != 'null' && $id_addon2 != 'null') {
            $array_addon = array('idaddon1' =>$id_addon1,'idaddon2' =>$id_addon2);
        
            $i = 0;
            $ancienne_addon = $this->executerRequete("SELECT id_addon FROM pivot_builds_addons WHERE id_build = :idbuild", [":idbuild"=>$id_build]);
            foreach ($ancienne_addon as $row) {
                $test[] = $row;
                $i = $i + 1;   
                if($row['id_addon'] != $array_addon['idaddon'.$i]) {
                    $perkbuildedit = $this->executerRequete("UPDATE pivot_builds_addons SET id_addon=:id_new_addon where id_build = :idbuild AND id_addon = :id_ancien_addon",[":id_new_addon"=>$array_addon['idaddon'.$i],":idbuild" => $id_build, ":id_ancien_addon"=>$row['id_addon']]);
                }
                if ($i > 1) {
                    $test4 = $row;
                } 
            }
            if(!isset($test)) {
                $insertbuildaddon1 = $this->executerRequete("INSERT INTO pivot_builds_addons (id_build, id_addon) VALUES (:id_build,:id_addon)",[':id_build'=>$id_build,':id_addon'=>$id_addon1]);
                $insertbuildaddon2 = $this->executerRequete("INSERT INTO pivot_builds_addons (id_build, id_addon) VALUES (:id_build,:id_addon)",[':id_build'=>$id_build,':id_addon'=>$id_addon2]);
            }
            if(!isset($test4)) {
                $insertbuildaddon2 = $this->executerRequete("INSERT INTO pivot_builds_addons (id_build, id_addon) VALUES (:id_build,:id_addon)",[':id_build'=>$id_build,':id_addon'=>$id_addon2]);
            }
        }
        elseif ($id_addon1 != 'null' || $id_addon2 != 'null') {
            if($id_addon1 != 'null') {
                $array_addon = array('idaddon1' =>$id_addon1);
        
                $i = 0;
                $ancienne_addon = $this->executerRequete("SELECT id_addon FROM pivot_builds_addons WHERE id_build = :idbuild", [":idbuild"=>$id_build]);
                foreach ($ancienne_addon as $row) {
                    $test1[] = $row;
                    $i = $i + 1;     
                    if($row['id_addon'] != $array_addon['idaddon'.$i]) {
                        $perkbuildedit = $this->executerRequete("UPDATE pivot_builds_addons SET id_addon=:id_new_addon where id_build = :idbuild AND id_addon = :id_ancien_addon",[":id_new_addon"=>$array_addon['idaddon'.$i],":idbuild" => $id_build, ":id_ancien_addon"=>$row['id_addon']]);
                    }
                }
                if(!isset($test1)) {
                    $insertbuildoffering = $this->executerRequete("INSERT INTO pivot_builds_addons (id_build, id_addon) VALUES (:id_build,:id_addon)",[':id_build'=>$id_build,':id_addon'=>$id_addon1]);
                }
            }
            if($id_addon2 != 'null') {
                $array_addon = array('idaddon2' =>$id_addon2);
        
                $i = 0;
                $ancienne_addon = $this->executerRequete("SELECT id_addon FROM pivot_builds_addons WHERE id_build = :idbuild", [":idbuild"=>$id_build]);
                foreach ($ancienne_addon as $row) {
                    $i = $i + 1;     
                    if($i >= 1 && $row['id_addon'] != $array_addon['idaddon'.$i]) {
                        echo "test";
                        $test2[] = $row;
                        $perkbuildedit = $this->executerRequete("UPDATE pivot_builds_addons SET id_addon=:id_new_addon where id_build = :idbuild AND id_addon = :id_ancien_addon",[":id_new_addon"=>$array_addon['idaddon'.$i],":idbuild" => $id_build, ":id_ancien_addon"=>$row['id_addon']]);
                    }
                }
                if(!isset($test2)) {
                    echo test;
                    $insertbuildoffering = $this->executerRequete("INSERT INTO pivot_builds_addons (id_build, id_addon) VALUES (:id_build,:id_addon)",[':id_build'=>$id_build,':id_addon'=>$id_addon2]);
                }
            }
        }
        return $response;

	}
    
}

