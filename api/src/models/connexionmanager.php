<?php

namespace deadbybuilder\models;

abstract class connexionmanager {

  const USERNAME="root";
  const PASSWORD="";
  const HOST="localhost";
  const DB="deadbybuilder";
  // Objet PDO d'accès à la BD
  private $bdd;

  // Exécute une requête SQL éventuellement paramétrée
  protected function executerRequete($sql, $params = null) {
    if ($params == null){
      $resultat = $this->connexion()->query($sql);    // exécution directe
      $resultat->setFetchMode(\PDO::FETCH_ASSOC);
    }
    else {
      $resultat = $this->connexion()->prepare($sql); // requête préparée
      $resultat->setFetchMode(\PDO::FETCH_ASSOC);
      foreach ($params as $placeholder => $value) {
        $resultat->bindValue($placeholder,$value);
      }

      $resultat->execute();


    }

    return $resultat;
  }

  // Renvoie un objet de connexion à la BD en initialisant la connexion au besoin
  private function connexion() {
      $username = self::USERNAME;
      $password = self::PASSWORD;
      $host = self::HOST;
      $db = self::DB;

    if ($this->bdd == null) {
      // Création de la connexion
      $this->bdd = new \PDO("mysql:dbname=$db;host=$host", $username, $password, array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION));
    }
    return $this->bdd;
  }

}

