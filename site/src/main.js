// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import App from './App'
import router from './router'
import axios from 'axios'
import {store} from './store/store'
import jquery from 'jquery'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap/dist/js/bootstrap.min.js"
import "bootstrap-vue/dist/bootstrap-vue.css"


Vue.use(BootstrapVue);

Vue.use({
install: function(Vue, options){
    Vue.prototype.$jQuery = require('jquery'); // you'll have this.$jQuery anywhere in your vue project
}
});

store.subscribe((mutation, state) => {
	localStorage.setItem('store', JSON.stringify(state));
});
window.axios = axios.create({

baseURL: 'http://localhost/deadbybuilder/',

});

Vue.config.productionTip = false
window.bus = new Vue();
/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store,
  beforeCreate(){
  	 this.$store.dispatch('initialiseStore')
  },
  components: { App },
  template: '<App/>'
})
