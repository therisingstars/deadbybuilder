import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import testroute from '@/components/testroute'
import perksperso from '@/components/perksperso'
import killers from '@/components/killers'
import survivors from '@/components/survivors'
import killer from '@/components/killer'
import survivor from '@/components/survivor'
import creabuild from '@/components/creabuild'
import formbuild from '@/components/formbuild'
import editbuild from '@/components/editbuild'
import killerbuild from '@/components/killerbuild'
import survivorbuild from '@/components/survivorbuild'
import buildkiller from '@/components/buildkiller'
import buildsurv from '@/components/buildsurv'
import NotFound from '@/components/NotFound'
import contact from '@/components/contact'
import profile from '@/components/profile'
import userprofile from '@/components/userprofile'
import Admin from '@/components/Admin'
import AdminUsers from '@/components/AdminUsers'
import AdminBuilds from '@/components/AdminBuilds'
import AdminCharacters from '@/components/AdminCharacters'
import AdminPerks from '@/components/AdminPerks'
import AdminItems from '@/components/AdminItems'
import AdminAddons from '@/components/AdminAddons'
import AdminOfferings from '@/components/AdminOfferings'
import AdminAddCharacter from '@/components/AdminAddCharacter'
import AdminAddPerk from '@/components/AdminAddPerk'
import AdminAddItem from '@/components/AdminAddItem'
import AdminAddOffering from '@/components/AdminAddOffering'
import AdminAddAddon from '@/components/AdminAddAddon'
import AdminEditCharacter from '@/components/AdminEditCharacter'
import AdminEditPerk from '@/components/AdminEditPerk'
import AdminEditItem from '@/components/AdminEditItem'
import AdminEditOffering from '@/components/AdminEditOffering'
import AdminEditAddon from '@/components/AdminEditAddon'
import AdminEditUser from '@/components/AdminEditUser'

Vue.use(Router)

export default new Router({
	mode : 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/testroute',
      name: 'testroute',
      component: testroute
    },
    {
      path: '/perksperso/:id',
      name: 'perksperso',
      component: perksperso
    },
    {
      path: '/killers',
      name: 'killers',
      component: killers
    },
    {
      path: '/survivors',
      name: 'survivors',
      component: survivors
    },
    {
      path: '/killer/:name',
      name: 'killer',
      component: killer
    },
    {
      path: '/survivor/:name',
      name: 'survivor',
      component: survivor
    },
    {
      path: '/creabuild',
      name: 'creabuild',
      component: creabuild
    }, 
    {
      path: '/creabuild/:name',
      name: 'formbuild',
      component: formbuild
    },
    {
      path: '/edit/build/:idbuild',
      name: 'modifbuild',
      component: editbuild
    },
    {   
      path: '/killer/:name/builds',
      name: 'killerbuild',
      component: killerbuild
    },
    {
      path: '/survivor/:name/builds',
      name: 'survivorbuild',
      component: survivorbuild
    },
    {
      path: '/killer/build/:name',
      name: 'buildkiller',
      component: buildkiller
    },
    {
      path: '/survivor/build/:name',
      name: 'buildsurv',
      component: buildsurv
    },
    {
      path: '/contact',
      name: 'contact',
      component: contact
    },
    {
      path: '/profile',
      name: 'profile',
      component: profile
    },
    {
      path: '/profile/:name',
      name: 'userprofile',
      component: userprofile
    },
    {
      path: '/admin',
      name: 'Admin',
      component: Admin
    },
    {
      path: '/admin/users',
      name: 'AdminUsers',
      component: AdminUsers
    },
    {
      path: '/admin/builds',
      name: 'AdminBuilds',
      component: AdminBuilds
    },
    {
      path: '/admin/characters',
      name: 'AdminCharacters',
      component: AdminCharacters
    },
    {
      path: '/admin/perks',
      name: 'AdminPerks',
      component: AdminPerks
    },
    {
      path: '/admin/items',
      name: 'AdminItems',
      component: AdminItems
    },
    {
      path: '/admin/addons',
      name: 'AdminAddons',
      component: AdminAddons
    },
    {
      path: '/admin/offerings',
      name: 'AdminOfferings',
      component: AdminOfferings
    },
    {
      path: '/admin/character/new',
      name: 'AdminAddCharacter',
      component: AdminAddCharacter
    },
    {
      path: '/admin/perk/new',
      name: 'AdminAddPerk',
      component: AdminAddPerk
    },
    {
      path: '/admin/item/new',
      name: 'AdminAddItem',
      component: AdminAddItem
    },
    {
      path: '/admin/addon/new',
      name: 'AdminAddAddon',
      component: AdminAddAddon
    },
    {
      path: '/admin/offering/new',
      name: 'AdminAddOffering',
      component: AdminAddOffering
    },
    {
      path: '/admin/edit/character/:id',
      name: 'AdminEditCharacter',
      component: AdminEditCharacter
    },
    {
      path: '/admin/edit/perk/:id',
      name: 'AdminEditPerk',
      component: AdminEditPerk
    },
    {
      path: '/admin/edit/item/:id',
      name: 'AdminEditItem',
      component: AdminEditItem
    },
    {
      path: '/admin/edit/offering/:id',
      name: 'AdminEditOffering',
      component: AdminEditOffering
    },
    {
      path: '/admin/edit/addon/:id',
      name: 'AdminEditAddon',
      component: AdminEditAddon
    },
    {
      path: '/admin/edit/user/:id',
      name: 'AdminEditUser',
      component: AdminEditUser
    },
    { 
      path: '/404', 
      component: NotFound 
    },  
    { 
      path: '*', 
      redirect: '/404' 
    }, 
  ]
})
