import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({

    state: {
      username:false,
      token:false,
      id_user:false,
      role:false,
      personnage:[],
      killerorsurv:''
    },

    getters: {

        getToken : (state) => {
            return state.token
        },
        getUsername : (state) => {
            return state.username
        },
        getUserId : (state) => {
            return state.id_user
        },
        getRole : (state) => {
            return state.role
        }
    },
    mutations: {
        setUsername : (state,username)=>{
            state.username = username
        },
        setToken : (state,token) => {
            state.token = token
        },
        setPersonnage : (state,personnage) => {
            state.personnage = personnage
        },
        setUserId : (state,id_user) => {
            state.id_user = id_user
        },
        setRole : (state,role)=>{
            state.role = role
        },
        setkillorsurv : (state,killerorsurv)=>{
            state.killerorsurv = killerorsurv
        },
        increment: (state,payload)=>{
        state.count = state.count+payload
        },
        initialiseStore(state){
            if(localStorage.getItem('store')) {
                this.replaceState(
                Object.assign(state, JSON.parse(localStorage.getItem('store')))
                );
            //localStorage.clear(); pour clear le storage afin de reset le state 
            }
        }
    },
    actions: {
        increment: (context,payload) => {

            setTimeout(function() {
               context.commit('increment',payload)
            },1000)

        },
        initialiseStore : (context) => {
            context.commit('initialiseStore')
        },
        setUsername : (context,username) => {

            context.commit('setUsername',username);
        },
        setPersonnage : (context,personnage) => {

            context.commit('setPersonnage',personnage);
        },
        setToken: (context,token)=>{

            context.commit('setToken',token);
        },
        setUserId: (context,id_user)=>{
            context.commit('setUserId',id_user);
        },
        setRole: (context,role)=>{
            context.commit('setRole',role);
        },
        setkillorsurv : (context,killerorsurv)=>{
            context.commit('setkillorsurv',killerorsurv);
        }
    }  
})
